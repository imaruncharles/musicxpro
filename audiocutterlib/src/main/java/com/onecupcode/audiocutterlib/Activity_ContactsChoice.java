package com.onecupcode.audiocutterlib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.onecupcode.audiocutterlib.adapter.Adapter_Contacts;
import com.onecupcode.audiocutterlib.models.Contacts;
import com.onecupcode.audiocutterlib.utils.PermissionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Activity_ContactsChoice extends AppCompatActivity implements SearchView.OnQueryTextListener, Adapter_Contacts.ContactsClickListener {

    private Uri mRingtoneUri;
    Toolbar mToolbar;
    RecyclerView mRecyclerView;
    Adapter_Contacts mAdapterContacts;
    List<Contacts> mData;



    String FILE_NAME = "FILE_NAME";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_choose_contacts);

        mRingtoneUri = (Uri) getIntent().getExtras().get(FILE_NAME);//Uri.parse(getIntent().getExtras().getString(FILE_NAME));
        mData = new ArrayList<>();
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.contacts);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mData = getContacts(this, "");
        mAdapterContacts = new Adapter_Contacts(this, mData,this);
        mRecyclerView.setAdapter(mAdapterContacts);
    
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search,menu);
        SearchView  mSearchView = (SearchView) menu.findItem(R.id.search).getActionView();
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setIconified(false);

        menu.findItem(R.id.search).setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                finish();
                return false;
            }
        });


/*
        menu.findItem(R.id.menu_search).expandActionView();
*/

        return true;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mData = getContacts(this, newText);
        mAdapterContacts.updateData(mData);
        return false;
    }


    private List<Contacts> getContacts(Context context,String searchQuery){
        String selection = "(DISPLAY_NAME LIKE \"%"+searchQuery+"%\")";
        List<Contacts> contactsModels = new ArrayList<Contacts>();

        Cursor cursor = context.getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.CUSTOM_RINGTONE, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.LAST_TIME_CONTACTED, ContactsContract.Contacts.STARRED, ContactsContract.Contacts.TIMES_CONTACTED, ContactsContract.Contacts.PHOTO_ID},
                selection, null,
                "STARRED DESC, TIMES_CONTACTED DESC, LAST_TIME_CONTACTED DESC, DISPLAY_NAME ASC");

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Contacts contactsModel = new Contacts(cursor.getString(2),
                        cursor.getString(0), cursor.getInt(6));
                contactsModels.add(contactsModel);
            } while (cursor.moveToNext());
        }

        return contactsModels;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onContackClicked(int position) {

        if (PermissionManager.checkAndRequestContactsPermissions(this)){
            Contacts contactsModel = mData.get(position);
            Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, contactsModel.getmContactId());
            ContentValues values = new ContentValues();
            values.put(ContactsContract.Contacts.CUSTOM_RINGTONE, mRingtoneUri.toString());
            getContentResolver().update(uri,values,null,null);
            String message = getResources().getText(R.string.Edit_Done_Toast).toString()+" "+contactsModel.getmName();

        /*            .update(uri, values, null, null)
            val message = resources.getText(R.string.Edit_Done_Toast).toString() +
                    " " +
                    contactsModel.mName*/
            Toast.makeText(this, message, Toast.LENGTH_SHORT)
                    .show();
            finish();
        }
    }
}
