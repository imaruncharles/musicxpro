package com.onecupcode.audioeditor.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.MotionEventCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.onecupcode.audioeditor.AudioModel;
import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.recylerhelper.ItemTouchHelperAdapter;
import com.onecupcode.audioeditor.recylerhelper.ItemTouchHelperViewHolder;
import com.onecupcode.audioeditor.recylerhelper.OnStartDragListener;

import java.util.Collections;
import java.util.List;


public class SelectedAudioAdapter extends RecyclerView.Adapter<SelectedAudioAdapter.ItemViewHolder>  implements ItemTouchHelperAdapter {


    private final List<AudioModel> audioModelList;
    private final OnStartDragListener mDragStartListener;

    public SelectedAudioAdapter(List<AudioModel> audioModels,OnStartDragListener mDragStartListener){
        this.mDragStartListener = mDragStartListener;
        this.audioModelList = audioModels;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.selected_audio_list_view, parent, false));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        holder.textView.setText(audioModelList.get(position).getaName());
        holder.handleView.setOnTouchListener((view, motionEvent) -> {
            if (MotionEventCompat.getActionMasked(motionEvent) == MotionEvent.ACTION_DOWN) {
                mDragStartListener.onStartDrag(holder);
            }
            return false;
        });
    }


    @Override
    public void onItemDismiss(int position) {
        audioModelList.remove(position);
        mDragStartListener.onItemRemovedByDrag();
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(audioModelList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public int getItemCount() {
        return audioModelList.size();
    }
    static class ItemViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {


        public final TextView textView;
        public final ImageButton handleView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.song_name);
            handleView = itemView.findViewById(R.id.dragim);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }


}

