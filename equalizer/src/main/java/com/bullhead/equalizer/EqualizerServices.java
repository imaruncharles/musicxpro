package com.bullhead.equalizer;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.media.audiofx.BassBoost;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class EqualizerServices extends IntentService {


    public Equalizer mEqualizer;
    public BassBoost bassBoost;
    public PresetReverb presetReverb;

    short   numberOfFrequencyBands;
    private int  audioSesionId;

    int y = 0;

    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.bullhead.equalizer.action.FOO";
    private static final String ACTION_BAZ = "com.bullhead.equalizer.action.BAZ";
    private static final String ACTION_EQ_ENABLE = "com.bullhead.equalizer.action.Enable";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.bullhead.equalizer.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.bullhead.equalizer.extra.PARAM2";
    public EqualizerServices() {
        super("EqualizerServices");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Settings.isEditing = true;

        if (Settings.equalizerModel == null){
            Settings.equalizerModel = new EqualizerModel();
            Settings.equalizerModel.setReverbPreset(PresetReverb.PRESET_NONE);
            Settings.equalizerModel.setBassStrength((short)(1000/19));
        }

        mEqualizer = new Equalizer(0,audioSesionId);
        bassBoost = new BassBoost(0,audioSesionId);
        bassBoost.setEnabled(Settings.isEqualizerEnabled);

        BassBoost.Settings bassBoostSettingTemp = bassBoost.getProperties();
        BassBoost.Settings bassBootSettings = new BassBoost.Settings(bassBoostSettingTemp.toString());
        bassBootSettings.strength = Settings.equalizerModel.getBassStrength();
        bassBoost.setProperties(bassBootSettings);

        //presetReverb

        presetReverb = new PresetReverb(0,audioSesionId);
        presetReverb.setPreset(Settings.equalizerModel.getReverbPreset());
        presetReverb.setEnabled(Settings.isEqualizerEnabled);

        mEqualizer.setEnabled(Settings.isEqualizerEnabled);

        if (Settings.presetPos==0){
            for (short bandIdx = 0; bandIdx < mEqualizer.getNumberOfBands(); bandIdx++){
                mEqualizer.setBandLevel(bandIdx,(short)Settings.seekbarpos[bandIdx]);
            }
        }else {
            mEqualizer.usePreset((short)Settings.presetPos);
        }

        if (!Settings.isEqualizerReloaded){
            int x = 0;

            if (bassBoost !=null){
                try {
                    x = ((bassBoost.getRoundedStrength() * 19) / 1000);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            if (presetReverb != null){
                try {
                    y = (presetReverb.getPreset() * 19) / 6;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            if (x==0){

            }
        }
    }

    public static void startActionEQ_Enable(Context context,boolean param1){
     Intent intent = new Intent(context,EqualizerServices.class);
     intent.putExtra(ACTION_EQ_ENABLE,param1);
     context.startService(intent);
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, EqualizerServices.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, EqualizerServices.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);
            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionBaz(param1, param2);
            }else if (ACTION_EQ_ENABLE.equals(action)){
                boolean isChecked = intent.getBooleanExtra(EXTRA_PARAM1,false);
                mEqualizer.setEnabled(isChecked);
                bassBoost.setEnabled(isChecked);
                presetReverb.setEnabled(isChecked);
                Settings.isEqualizerEnabled = isChecked;
                Settings.equalizerModel.setEqualizerEnabled(isChecked);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}