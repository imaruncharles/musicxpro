package com.onecupcode.audiocutterlib.utils;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;

public class ViewUtil {



    @SuppressLint("ClickableViewAccessibility")
  public static void SetOntouchListener(View view){

        view.setOnTouchListener((view1, motionEvent) -> {

            int action = motionEvent.getAction();
            if (action == MotionEvent.ACTION_DOWN) {
                view1.animate().scaleXBy(-0.05f).setDuration(100).start();
                view1.animate().scaleYBy(-0.05f).setDuration(100).start();
                return false;
            } else if (action == MotionEvent.ACTION_UP) {
                view1.animate().cancel();
                view1.animate().scaleX(1f).setDuration(100).start();
                view1.animate().scaleY(1f).setDuration(100).start();
                return false;
            }


            return false;
        });
    }
}
