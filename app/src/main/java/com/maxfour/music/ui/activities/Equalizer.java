package com.maxfour.music.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import com.bullhead.equalizer.EqualizerFragment;
import com.maxfour.music.R;
import com.maxfour.music.helper.MusicPlayerRemote;


public class Equalizer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equalizer);


        try {
            int sessionId = MusicPlayerRemote.getAudioSessionId();
            // mediaPlayer.setLooping(true);
            EqualizerFragment equalizerFragment = EqualizerFragment.newBuilder()
                    .setAccentColor(Color.parseColor("#4caf50"))
                    .setAudioSessionId(sessionId)
                    .build();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.eqFrame, equalizerFragment)
                    .commit();
        }catch (Exception ignore){
            finish();
            Toast.makeText(this,"You must play a song first!",Toast.LENGTH_SHORT).show();

        }


    }
}