package com.onecupcode.audioeditor.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.onecupcode.audioeditor.AudioModel;
import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.miniplayer.MiniPlayer;

import java.util.ArrayList;
import java.util.List;



public class MergeAdapter  extends RecyclerView.Adapter<MergeAdapter.ViewHolder> implements Filterable {


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
             String charString = charSequence.toString();
             if (charString.isEmpty()){
                 audioModelFilter = audioModel;
             }else {
                 List<AudioModel> filteredList = new ArrayList<>();
                 for (AudioModel row : audioModel){
                     if (row.getaName().toLowerCase().contains(charString.toLowerCase()) || row.getaArtist().toLowerCase().contains(charString.toLowerCase())){
                         filteredList.add(row);
                     }
                 }
                 audioModelFilter = filteredList;
             }

             FilterResults filterResults = new FilterResults();
             filterResults.values = audioModelFilter;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                audioModelFilter = (ArrayList<AudioModel>)filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnSongClickListener {
        void onSongItemClick(AudioModel audioModel);
        void onSongPlay(AudioModel audioModel);
        void onMoreMenuSelected(AudioModel audioModel,int flagt);
    }

    OnSongClickListener onSongClickListener;
    List<AudioModel> audioModel;
    List<AudioModel> audioModelFilter;
    boolean isPlay;
    public MergeAdapter(List<AudioModel> audioModel,OnSongClickListener onSongClickListener, boolean isPlay){
       this.audioModel = audioModel;
       this.onSongClickListener = onSongClickListener;
       this.audioModelFilter = audioModel;
       this.isPlay = isPlay;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.merger_view,parent,false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.song_name.setText(""+audioModelFilter.get(position).getaName());
        if (!isPlay){
            holder.moreBtn.setVisibility(View.VISIBLE);
            holder.playBtn.setImageResource(R.drawable.musicgradient1);
            holder.moreBtn.setOnClickListener(view -> {

                if (checkAndRequestContactsPermissions((Activity) view.getContext())){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.System.canWrite(view.getContext())) {
                            openAndroidPermissionsMenu(view.getContext());

                        }else {
                            popupMenu(view,position);
                        }
                    }else {
                        popupMenu(view,position);
                    }
                }
            });
        }else {
            holder.moreBtn.setVisibility(View.GONE);
            holder.playBtn.setOnClickListener(view -> onSongClickListener.onSongPlay(audioModelFilter.get(position)));
        }

    }

    @Override
    public int getItemCount() {
        return audioModelFilter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView song_name;
        AppCompatImageView moreBtn;
        ImageView playBtn;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            song_name = itemView.findViewById(R.id.song_name);
            playBtn = itemView.findViewById(R.id.playBtn);
            moreBtn = itemView.findViewById(R.id.moreBtn);
            itemView.setOnClickListener(v -> onSongClickListener.onSongItemClick(audioModelFilter.get(getAdapterPosition())));
        }
    }

    private void popupMenu(View view,int pos){
        PopupMenu popupMenu = new PopupMenu(view.getContext(),view);
        popupMenu.inflate(R.menu.audiocutter_menu);
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            if (menuItem.getItemId()==R.id.set_ringtone){
                    onSongClickListener.onMoreMenuSelected(audioModelFilter.get(pos),0);
            }else if (menuItem.getItemId() == R.id.assign_to_contact){
                onSongClickListener.onMoreMenuSelected(audioModelFilter.get(pos),1);
            }else if (menuItem.getItemId() == R.id.set_notification){
                onSongClickListener.onMoreMenuSelected(audioModelFilter.get(pos),2);
            }else if (menuItem.getItemId() == R.id.set_alarm){
                onSongClickListener.onMoreMenuSelected(audioModelFilter.get(pos),3);
            }else if (menuItem.getItemId() == R.id.shareAudio){
                onSongClickListener.onMoreMenuSelected(audioModelFilter.get(pos),4);
            }
            return false;
        });

        popupMenu.show();
    }


    private void openAndroidPermissionsMenu(Context context) {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + context.getPackageName()));
        context.startActivity(intent);
    }

    public  boolean checkAndRequestContactsPermissions(Activity activity) {
        int modifyAudioPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        if (modifyAudioPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS},
                    105);
            return false;
        }
        return true;
    }
}
