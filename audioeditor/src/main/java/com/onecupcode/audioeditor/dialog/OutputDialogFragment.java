package com.onecupcode.audioeditor.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.onecupcode.audioeditor.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class OutputDialogFragment extends DialogFragment {


    public OutputDialogFragment(){

    }

   public interface OutputDialogListener{
        void onOutPutDialogClosed(String fileName,String format,boolean bitRateFlag,String bitrate,String sampleRate);
        void onOutPutDialogCanceled();
    }

    OutputDialogListener outputDialogListener;

    public static OutputDialogFragment newInstance(String flag,String title,String fileName){
        OutputDialogFragment outputDialogFragment = new OutputDialogFragment();
        Bundle args = new Bundle();
        args.putString("flag",flag);
        args.putString("okBtn",title);
        args.putString("fileName",fileName);
        outputDialogFragment.setArguments(args);
        return outputDialogFragment;
    }

    public void initOutPutDialogListener(OutputDialogListener outputDialogListener){
        this.outputDialogListener = outputDialogListener;
    }

  /*  @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.file_name_dialog,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }*/


    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        String okBtn = getArguments().getString("okBtn");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.enter_file_name);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.file_name_dialog,null,false);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        EditText fileNameEdit = view.findViewById(R.id.fileNameText);
        Spinner formatSpinner = view.findViewById(R.id.formatSpinner);
        Spinner bitRateSpinner = view.findViewById(R.id.bitrateSpinner);
        Spinner sampleRateSpinner = view.findViewById(R.id.sampleRateSpinner);
        CheckBox checkBox = view.findViewById(R.id.bitrateCheckBox);
        LinearLayout bitRateLayout = view.findViewById(R.id.bitRateLayout);
        LinearLayout forLayout = view.findViewById(R.id.formatLinear);

        if (getArguments().getString("flag").equals("cut")){
            forLayout.setVisibility(View.GONE);
        }

        bitRateLayout.setVisibility(View.GONE);
        checkBox.setVisibility(View.GONE);

        checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                bitRateLayout.setVisibility(View.VISIBLE);
            }else {
                bitRateLayout.setVisibility(View.INVISIBLE);
            }
        });

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM_dd_yyyy_hh_mm_ss_SS");
        fileNameEdit.setText(""+getArguments().getString("fileName")+" "+simpleDateFormat.format(new Date()));

        String[] FORMAT_ARRAY = getResources().getStringArray(R.array.audio_format);
        String[] BIT_RATE_ARRAY = getResources().getStringArray(R.array.bitrate_array);
        String[] SAMPLE_RATE_ARRAY = getResources().getStringArray(R.array.sample_rate_array);

        ArrayAdapter<String> bit_rate_adapter = new ArrayAdapter<>(getActivity(),R.layout.spinner_text,BIT_RATE_ARRAY);
        ArrayAdapter<String> sample_rate_adapter = new ArrayAdapter<>(getActivity(),R.layout.spinner_text,SAMPLE_RATE_ARRAY);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text,FORMAT_ARRAY);
        //  arrayAdapter.setDropDownViewResource(R.drawable.ic_baseline_arrow_drop_down_24);
        formatSpinner.setAdapter(arrayAdapter);
        bitRateSpinner.setAdapter(bit_rate_adapter);
        sampleRateSpinner.setAdapter(sample_rate_adapter);


        alertDialogBuilder.setNegativeButton(R.string.cancel, (dialogInterface, i) ->{
            outputDialogListener.onOutPutDialogCanceled();
            dialogInterface.dismiss();
        });

        alertDialogBuilder.setPositiveButton(okBtn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (fileNameEdit.getText().toString().trim().equals("")){
                    fileNameEdit.setError("File Name is Empty");
                }else {
                    fileNameEdit.getText().toString().trim();
                    outputDialogListener.onOutPutDialogClosed(
                            ""+fileNameEdit.getText().toString().trim(),
                            ""+formatSpinner.getSelectedItem().toString(),
                            checkBox.isChecked(),
                            ""+BIT_RATE_ARRAY[bitRateSpinner.getSelectedItemPosition()],
                            ""+SAMPLE_RATE_ARRAY[sampleRateSpinner.getSelectedItemPosition()].replace(" Hz","")

                    );
                    dialogInterface.dismiss();
                }

                dialogInterface.dismiss();
            }
        });

        return alertDialogBuilder.create();//super.onCreateDialog(savedInstanceState);
    }
}
