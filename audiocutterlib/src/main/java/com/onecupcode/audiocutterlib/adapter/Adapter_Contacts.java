package com.onecupcode.audiocutterlib.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.onecupcode.audiocutterlib.R;
import com.onecupcode.audiocutterlib.models.Contacts;

import java.util.List;

public class Adapter_Contacts extends RecyclerView.Adapter<Adapter_Contacts.ItemHolder> {
    Context context;
    List<Contacts> mData;

   public interface ContactsClickListener {
        void onContackClicked(int position);
    }

    ContactsClickListener contactsClickListener;

    public Adapter_Contacts(Context context, List<Contacts> mData,ContactsClickListener contactsClickListener){
        this.context = context;
        this.mData = mData;
        this.contactsClickListener = contactsClickListener;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contacts,parent,false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {

        holder.ContactName.setText(""+mData.get(position).getmName());

        if (mData.get(position).getmPhotoID()!=0){
            holder.ContactImg.setImageBitmap(queryContactImage(mData.get(position).getmPhotoID()));
        }else {
            holder.ContactImg.setImageResource(R.drawable.ic_baseline_person_24);
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private Bitmap queryContactImage(int imageDataRow){

        Cursor c = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{ContactsContract.CommonDataKinds.Photo.PHOTO}, ContactsContract.Data._ID + "=?",new String[] {Integer.toString(imageDataRow)}, null);

       byte[] imageBytes = null;

       if (c !=null){
           if (c.moveToFirst()){
               imageBytes = c.getBlob(0);
           }
           c.close();
       }
       if (imageBytes!=null){
         return   BitmapFactory.decodeByteArray(imageBytes,0,imageBytes.length);
       }else {
           return null;
       }

    }


    public void  updateData(List<Contacts> data){
        this.mData = data;
        notifyDataSetChanged();
    }

    class ItemHolder extends RecyclerView.ViewHolder{

        AppCompatTextView ContactName;
        AppCompatImageView ContactImg;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            ContactImg = itemView.findViewById(R.id.imageViewUserPic);
            ContactName = itemView.findViewById(R.id.textViewUser);

            itemView.setOnClickListener(view -> contactsClickListener.onContackClicked(getAdapterPosition()));
        }
    }
}
