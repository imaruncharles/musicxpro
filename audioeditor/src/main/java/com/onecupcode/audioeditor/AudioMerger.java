package com.onecupcode.audioeditor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.DialogCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import com.google.android.material.dialog.MaterialDialogs;
import com.onecupcode.audioeditor.adapter.SelectedAudioAdapter;
import com.onecupcode.audioeditor.miniplayer.MiniPlayer;
import com.onecupcode.audioeditor.recylerhelper.OnStartDragListener;
import com.onecupcode.audioeditor.recylerhelper.SimpleItemTouchHelperCallback;
import com.onecupcode.audioeditor.worker.AudioMergeWork;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;



public class AudioMerger extends AppCompatActivity implements  View.OnClickListener, OnStartDragListener {

    private  final int PICKFILE_RESULT_CODE = 100;
    RecyclerView recyclerView_selected;
    SelectedAudioAdapter mergeSelectionAdapter;
    List<AudioModel> selectedAudioList;
    String FILE_NAME = "musix_xpro_merger" ;
    private ItemTouchHelper mItemTouchHelper;
    Menu menu;
    AppCompatButton add_audio,merge_audio;




    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_merger);


        isStoragePermissionGranted();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.merger);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24);


        CardView trailView = findViewById(R.id.trail_view); //.from(getApplicationContext()).inflate(R.layout.trail_view,null,false);

        if (getIntent().getStringExtra("build").equals("com.onecupcode.musicxpro.paid")){
            trailView.setVisibility(View.GONE);
        }

        trailView.setOnClickListener(view -> goToMusicXProPrime());

        selectedAudioList = new ArrayList<>();
        recyclerView_selected = findViewById(R.id.recyclerView_selected);
        mergeSelectionAdapter = new SelectedAudioAdapter(selectedAudioList,this);
        recyclerView_selected.setAdapter(mergeSelectionAdapter);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mergeSelectionAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(recyclerView_selected);

        add_audio = findViewById(R.id.add_audio);
        merge_audio = findViewById(R.id.merge_audio);

        add_audio.setOnClickListener(this);
        merge_audio.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        if (v.getId()==R.id.merge_audio){
            if (selectedAudioList.size()>0)
            createFileNameDialog("Enter File Name");
        }else if (v.getId() == R.id.add_audio){
            startActivityForResult(new Intent(getApplicationContext(),AudioFileChooser.class),101);
        }
    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onItemRemovedByDrag() {
    }

    /*
    * -i "concat:20181021_080743.MP3|20181021_090745.MP3|20181021_100745.MP3" -acodec copy 20181021.mp3
    * */

    public void processSelectedFile(){
        List<String> commandList =  new ArrayList<>();
       // StringBuilder commend = new StringBuilder("-i");
        if (selectedAudioList.size()>0){
            String outputPath = getOutPutFilePath();

            if(new File(outputPath).exists()){
                createFileNameDialog("Given File name Already Exist, Try new File Name");
                return;
            }
            StringBuilder fileIndexBuilder = new StringBuilder();
            for (int i=0;i<selectedAudioList.size();i++){
                commandList.add("-i");
                commandList.add(selectedAudioList.get(i).getaPath());
                @SuppressLint("DefaultLocale") String fib = String.format("[%d:0]",i);
                fileIndexBuilder.append(fib);
                //commend.append(" ").append(audioModel.getaPath());
            }
            fileIndexBuilder.append("concat=n=").append(selectedAudioList.size()).append(":v=0:a=1[out]");
            commandList.add("-filter_complex");
           // commandList.add("[0:0][1:0][2:0][3:0]concat=n=4:v=0:a=1[out]");
            commandList.add(fileIndexBuilder.toString());
            commandList.add("-map");
            commandList.add("[out]");
            commandList.add(outputPath);
            //commend.append(" -acodec copy ").append(getOutPutFilePath());

            String[] command = new String[commandList.size()];
            for (int i=0 ; i < commandList.size() ; i++){
                command[i] = commandList.get(i);
            }
           // ffmpegTestTaskQuit(command);
            WorkRequest mergeWorkRequest = new OneTimeWorkRequest.Builder(AudioMergeWork.class)
                    .setInputData(createInputData(command,outputPath))
                    .build();

            WorkManager.getInstance(getApplicationContext())
                    .enqueue(mergeWorkRequest);

            showWorkInfoProgress(FILE_NAME,outputPath,mergeWorkRequest.getId());
        }
//        startActivity(new Intent(getApplicationContext(),ExampleActivity.class).putExtra("commend",commend.toString()));
    }

    private Data createInputData(String[] command,String outputPath){
        Data.Builder builder = new Data.Builder();
        builder.putStringArray("command",command);
        builder.putString("filename",FILE_NAME);
        builder.putString("outputpath",outputPath);
        return builder.build();


    }


    private String getOutPutFilePath(){

        return UriUtils.makeFilename("Audio_Merger",FILE_NAME,"."+FORMAT);
    }

    @SuppressLint("SetTextI18n")
    private void showWorkInfoProgress(String fileName,String outputPath,UUID uuid){


        AlertDialog.Builder alerdialog = new AlertDialog.Builder(AudioMerger.this);
        alerdialog.setTitle("Merging Audio...");
        alerdialog.setMessage(fileName);
        alerdialog.setView(R.layout.progress_dialog);
        alerdialog.setCancelable(false);
        alerdialog.setPositiveButton("Do In Background",(dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        AlertDialog aler = alerdialog.show();

        WorkManager.getInstance(getApplicationContext())
                // requestId is the WorkRequest id
                .getWorkInfoByIdLiveData(uuid)
                .observe(AudioMerger.this, workInfo -> {
                    if (workInfo != null) {
                        if (workInfo.getState()== WorkInfo.State.SUCCEEDED){
                            Intent intent = new Intent(AudioMerger.this, MiniPlayer.class);
                            intent.putExtra("path",outputPath);
                            startActivity(intent);
                            if (alerdialog!=null){
                                aler.dismiss();
                            }

                        }else if (workInfo.getState() == WorkInfo.State.CANCELLED || workInfo.getState() == WorkInfo.State.FAILED){
                            if (alerdialog!=null){
                                alerdialog.setTitle("Merger Failed");
                                alerdialog.setMessage("retry");
                            }
                        }
                        // Do something with progress
                    }
                });
    }


    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }



    String FORMAT = "mp3";
    @SuppressLint({"SetTextI18n", "ResourceType"})
    private void createFileNameDialog(String msg){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AudioMerger.this);
        alertDialog.setTitle("File Name");
        alertDialog.setMessage(msg);
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.file_name_dialog,null,false);
        alertDialog.setView(view);
        alertDialog.setCancelable(false);
        EditText fileNameEdit = view.findViewById(R.id.fileNameText);
        Spinner formatSpinner = view.findViewById(R.id.formatSpinner);
        String[] FORMAT_ARRAY = getResources().getStringArray(R.array.audio_format);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_text,FORMAT_ARRAY);
      //  arrayAdapter.setDropDownViewResource(R.drawable.ic_baseline_arrow_drop_down_24);
        formatSpinner.setAdapter(arrayAdapter);

        formatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                FORMAT = FORMAT_ARRAY[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM_dd_yyyy_hh_mm_ss_SS");
        fileNameEdit.setText("Merger "+simpleDateFormat.format(new Date()));

        alertDialog.setPositiveButton("Merge", (dialogInterface, i) -> {

            if (fileNameEdit.getText().toString().trim().equals("")){
                fileNameEdit.setError("File Name Empty");
            }else {
                FILE_NAME = ""+fileNameEdit.getText().toString().trim();
                processSelectedFile();
                selectedAudioList.clear();
                mergeSelectionAdapter.notifyDataSetChanged();
                dialogInterface.dismiss();
            }

        });
        alertDialog.setNegativeButton("Cancel",(dialogInterface, i) -> {
           dialogInterface.dismiss();
        });
        alertDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.search_menu,menu);
        hideAndShow(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }else
        if (item.getItemId() == R.id.menu_search){
            return true;
        }
        else if (item.getItemId()==R.id.folder){
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("audio/*");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            }
            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (PICKFILE_RESULT_CODE==requestCode && resultCode == RESULT_OK){
            if (data.getData()!=null)
            {
                // Retrieve the phone number from the NUMBER column
                File audio_file = new File(Objects.requireNonNull(UriUtils.getPathFromUri(getApplicationContext(), data.getData())));
                AudioModel audioModel = new AudioModel();
                audioModel.setaPath(audio_file.getAbsolutePath());
                audioModel.setaName(audio_file.getName());
                selectedAudioList.add(audioModel);
                mergeSelectionAdapter.notifyDataSetChanged();
            }else if (data.getClipData()!=null){
                if (data.getClipData()!=null){

                    for (int i =0 ;i < data.getClipData().getItemCount() ; i++){

                        File audio_file = new File(Objects.requireNonNull(UriUtils.getPathFromUri(getApplicationContext(), data.getClipData().getItemAt(i).getUri())));
                        AudioModel audioModel = new AudioModel();
                        audioModel.setaPath(audio_file.getAbsolutePath());
                        audioModel.setaName(audio_file.getName());
                        selectedAudioList.add(audioModel);
                    }
                    mergeSelectionAdapter.notifyDataSetChanged();
                }
            }
        }
        else if (101 == requestCode && resultCode == RESULT_OK){
            List<AudioModel> audioModelList = data.getParcelableArrayListExtra("audioFile");
            selectedAudioList.addAll(audioModelList);
            mergeSelectionAdapter.notifyDataSetChanged();
        }
    }


    private void hideAndShow(boolean flag){
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        menuItem.setVisible(flag);
    }


    private void goToMusicXProPrime(){
        try {
            Uri uriAlter = Uri.parse("http://play.google.com/store/apps/details?id=com.onecupcode.musicxpro.paid");
            Intent intent = new Intent(Intent.ACTION_VIEW,uriAlter);
            if (intent.resolveActivity(getPackageManager())!=null){
                startActivity(intent);
            }
        }catch (ActivityNotFoundException ignore){

        }
    }


}