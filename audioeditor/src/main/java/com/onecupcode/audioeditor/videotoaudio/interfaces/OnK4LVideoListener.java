package com.onecupcode.audioeditor.videotoaudio.interfaces;

public interface OnK4LVideoListener {

    void onVideoPrepared(int maxDuration);
}
