package com.onecupcode.audiocutterlib.models;

public class Contacts {

    String mName , mContactId;
    int mPhotoID;
    public Contacts(String mName , String mContactId, int mPhotoID){
        this.mName = mName;
        this.mContactId = mContactId;
        this.mPhotoID = mPhotoID;
    }

    public String getmName() {
        return mName;
    }

    public String getmContactId() {
        return mContactId;
    }

    public int getmPhotoID() {
        return mPhotoID;
    }
}
