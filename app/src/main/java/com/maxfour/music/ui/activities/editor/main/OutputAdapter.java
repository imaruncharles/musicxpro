package com.maxfour.music.ui.activities.editor.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.maxfour.music.R;
import com.maxfour.music.model.Song;

import java.util.ArrayList;
import java.util.List;

public class OutputAdapter extends RecyclerView.Adapter<OutputAdapter.OutHolder>{

    List<Song> songList;
    OutputAdapter(){
       songList = new ArrayList<>();
    }

    public void loadSongList(List<Song> songList){
        this.songList = songList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OutHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull OutHolder holder, int position) {
        holder.title.setText(songList.get(position).title);
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }

    class OutHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView details;

        public OutHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
        }
    }
}
