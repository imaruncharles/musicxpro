package com.onecupcode.audioeditor;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.onecupcode.audioeditor.adapter.MergeAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import timber.log.Timber;

public class AudioFileChooser extends AppCompatActivity implements View.OnClickListener, MergeAdapter.OnSongClickListener {

    private  final int PICKFILE_RESULT_CODE = 100;
    RecyclerView recyclerView_merger;
    MergeAdapter mergeAdapter;
    List<AudioModel> audioModelList;
    Button audioCountBtn;

    SearchView searchView;
    Menu menu;
    ArrayList<AudioModel> selectedAudioList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_file_chooser);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.select_audio);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24);

        audioModelList = new ArrayList<>();
        selectedAudioList = new ArrayList<>();
        recyclerView_merger = findViewById(R.id.recyclerView_merger);

        mergeAdapter = new MergeAdapter(audioModelList,this,true);
        audioCountBtn = findViewById(R.id.count_song_btn);
        recyclerView_merger.setAdapter(mergeAdapter);
        audioCountBtn.setOnClickListener(this);
        audioCountBtn.setText("Audio (0)");

        new AsyncTaskgetAllAudio().execute();

    }


    @Override
    public void onBackPressed() {
       // super.onBackPressed();

        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("audioFile",  selectedAudioList);
        setResult(RESULT_OK,intent);
        finish();

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSongItemClick(AudioModel position) {
        selectedAudioList.add(position);
        audioCountBtn.setText("Audio ("+selectedAudioList.size()+")");
    }

    @Override
    public void onSongPlay(AudioModel audioModel) {
        initMediaPlayer(audioModel.getaPath());
    }

    @Override
    public void onMoreMenuSelected(AudioModel audioModel, int flagt) {

    }



    @Override
    public void onClick(View v) {

        if (v.getId()==R.id.count_song_btn){
            closeSelectedView();
        }
    }

    @SuppressLint("SetTextI18n")
    private void closeSelectedView(){
        onBackPressed();
    }

    @SuppressLint("StaticFieldLeak")
    class AsyncTaskgetAllAudio extends AsyncTask<Void,Void,List<AudioModel>> {

        @Override
        protected List<AudioModel> doInBackground(Void... voids) {
            return getAllAudioFromDevice(getApplicationContext());
        }

        @Override
        protected void onPostExecute(List<AudioModel> audioModels) {
            super.onPostExecute(audioModels);

            audioModelList = audioModels;
            mergeAdapter = new MergeAdapter(audioModelList, AudioFileChooser.this,true);
            recyclerView_merger.setAdapter(mergeAdapter);
            mergeAdapter.notifyDataSetChanged();
        }
    }

    public List<AudioModel> getAllAudioFromDevice(final Context context) {
        final List<AudioModel> tempAudioList = new ArrayList<>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.TITLE, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST, MediaStore.Audio.Media.DURATION,MediaStore.Audio.Media._ID};
        Cursor c = context.getContentResolver().query(uri, projection, MediaStore.Audio.Media.DATA + "!=0", null, null);

        if (c != null) {
            while (c.moveToNext()) {
                AudioModel audioModel = new AudioModel();
                String path = c.getString(0);
                String name = c.getString(1);
                String album = c.getString(2);
                String artist = c.getString(3);
                String duration = c.getString(4);
                int ID = c.getInt(5);
                audioModel.setaName(name);
                audioModel.setaAlbum(album);
                audioModel.setaArtist(artist);
                audioModel.setaPath(path);
                audioModel.setDuration(duration);
                audioModel.setID(ID);

                Timber.e(" Album :%s", album);
                Timber.e(" Artist :%s", artist);

                tempAudioList.add(audioModel);
            }
            c.close();
        }

        return tempAudioList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.search_menu,menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mergeAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mergeAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }else
        if (item.getItemId() == R.id.menu_search){
            return true;
        }
        else if (item.getItemId()==R.id.folder){
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("audio/*");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            }
            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (PICKFILE_RESULT_CODE==requestCode && resultCode == RESULT_OK){
            if (data.getData()!=null)
            {
                // Retrieve the phone number from the NUMBER column
                File audio_file = new File(Objects.requireNonNull(UriUtils.getPathFromUri(getApplicationContext(), data.getData())));
                AudioModel audioModel = new AudioModel();
                audioModel.setaPath(audio_file.getAbsolutePath());
                audioModel.setaName(audio_file.getName());
                selectedAudioList.add(audioModel);
                audioCountBtn.setText("Audio ("+selectedAudioList.size()+")");
            }else if (data.getClipData()!=null){
                if (data.getClipData()!=null){

                    for (int i =0 ;i < data.getClipData().getItemCount() ; i++){

                        File audio_file = new File(Objects.requireNonNull(UriUtils.getPathFromUri(getApplicationContext(), data.getClipData().getItemAt(i).getUri())));
                        AudioModel audioModel = new AudioModel();
                        audioModel.setaPath(audio_file.getAbsolutePath());
                        audioModel.setaName(audio_file.getName());
                        selectedAudioList.add(audioModel);
                    }
                    audioCountBtn.setText("Audio ("+selectedAudioList.size()+")");
                }
            }
        }

    }

    MediaPlayer mediaPlayer ;

    private void initMediaPlayer(String path) {
        try {
            if (mediaPlayer!=null){
                mediaPlayer.stop();
                mediaPlayer.release();
            }
            mediaPlayer = MediaPlayer.create(getApplicationContext(),Uri.fromFile(new File(path)));
            mediaPlayer.start();
        }catch (Exception ignore){
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
}