package com.maxfour.music.ui.activities.editor.main;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maxfour.music.R;

import java.io.File;

public class AuioCutter extends Fragment {

    private AuioCutterViewModel mViewModel;
    RecyclerView audioCutterRecyclerView;

    OutputAdapter outputAdapter;



    public static AuioCutter newInstance() {
        return new AuioCutter();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.auio_cutter_fragment, container, false);

        audioCutterRecyclerView = view.findViewById(R.id.audioCutterRecyclerView);
        outputAdapter = new OutputAdapter();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(AuioCutterViewModel.class);
        audioCutterRecyclerView.setAdapter(outputAdapter);

        File file = Environment.getExternalStorageDirectory();
        String path = file.getPath() +"/Music_XPro/Audio_Cutter";
       File makeFiles =  new File(path);
        if (makeFiles.exists()){
            makeFiles.mkdirs();
        }

        mViewModel.getMutableLiveData(getContext(),"/Music_XPro/Audio_Cutter");

        mViewModel.mutableLiveData.observe(getViewLifecycleOwner(),songs -> {
            outputAdapter.loadSongList(songs);
        });

    }

}