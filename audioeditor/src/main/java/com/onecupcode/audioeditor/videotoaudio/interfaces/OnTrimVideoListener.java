package com.onecupcode.audioeditor.videotoaudio.interfaces;

import android.net.Uri;

public interface OnTrimVideoListener {

    void onTrimStarted();

    void getResult(final Uri uri);

    void cancelAction();

    void onError(final String message);

    void getVidoInfoDetails(int startTime,int endTime);
}
