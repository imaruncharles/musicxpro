package com.onecupcode.audioeditor.audiocutter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onecupcode.audioeditor.AudioFileChooser;
import com.onecupcode.audioeditor.AudioModel;
import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.UriUtils;
import com.onecupcode.audioeditor.adapter.MergeAdapter;
import com.onecupcode.audioeditor.adapter.SelectedAudioAdapter;
import com.onecupcode.audioeditor.videotoaudio.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import timber.log.Timber;

public class AudioListActivity extends AppCompatActivity implements MergeAdapter.OnSongClickListener {

    private final int PICKFILE_RESULT_CODE = 100;
    RecyclerView audioListRecycler;
    MergeAdapter mergeAdapter;
    List<AudioModel> audioModelList = new ArrayList<>();
    SearchView searchView;
    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_audio_list);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24);
        getSupportActionBar().setTitle("Audio Cutter");

        CardView trailView = findViewById(R.id.trail_view); //.from(getApplicationContext()).inflate(R.layout.trail_view,null,false);

        if (getIntent().getStringExtra("build").equals("com.onecupcode.musicxpro.paid")){
            trailView.setVisibility(View.GONE);
        }

        trailView.setOnClickListener(view -> goToMusicXProPrime());


        audioListRecycler = findViewById(R.id.audio_list_recycler);
        mergeAdapter = new MergeAdapter(audioModelList, this,false);

        new AsyncTaskAllAudio().execute();

    }

    @Override
    public void onSongItemClick(AudioModel audioModel) {
        startActivity(audioModel.getaPath());
    }

    @Override
    public void onSongPlay(AudioModel audioModel) {
       initMediaPlayer(audioModel.getaPath());
    }

    private static final int REQUEST_CODE_CHOOSE_CONTACT = 2;


    @Override
    public void onMoreMenuSelected(AudioModel audioModel, int flagt) {

        Uri uriPath = Utils.getSongFileUri(audioModel.getID());

        if (flagt==0){
            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_RINGTONE,uriPath);
            Toast.makeText(
                    AudioListActivity.this,
                    R.string.default_ringtone_success_message,
                    Toast.LENGTH_SHORT)
                    .show();
        }else if (flagt == 1){

            try {
                //Go to the choose contact activity
                Intent intent = new Intent(Intent.ACTION_EDIT, uriPath);
                intent.setClassName(
                        getPackageName(),
                        ChooseContactActivity.class.getName());
                startActivityForResult(intent, REQUEST_CODE_CHOOSE_CONTACT);
            } catch (Exception e) {
            }

        }else if (flagt == 2){
            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_NOTIFICATION,uriPath);
            Toast.makeText(
                    AudioListActivity.this,
                    R.string.default_notification_success_message,
                    Toast.LENGTH_SHORT)
                    .show();
        }else if (flagt == 3){
            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_ALARM,uriPath);
            Toast.makeText(
                    AudioListActivity.this,
                    R.string.default_alarm_success,
                    Toast.LENGTH_SHORT)
                    .show();
        } else if (flagt == 4){
           // FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, new File(getIntent().getStringExtra("path")));;
            shareAudioFile(uriPath);
        }
    }


    @SuppressLint("StaticFieldLeak")
    class AsyncTaskAllAudio extends AsyncTask<Void, Void, List<AudioModel>> {

        @Override
        protected List<AudioModel> doInBackground(Void... voids) {
            return getAllAudioFromDevice(getApplicationContext());
        }

        @Override
        protected void onPostExecute(List<AudioModel> audioModels) {
            super.onPostExecute(audioModels);

            audioModelList = audioModels;
            mergeAdapter = new MergeAdapter(audioModelList, AudioListActivity.this,false);
            audioListRecycler.setAdapter(mergeAdapter);
            mergeAdapter.notifyDataSetChanged();
        }
    }

    public List<AudioModel> getAllAudioFromDevice(final Context context) {
        final List<AudioModel> tempAudioList = new ArrayList<>();

        String orderBy = MediaStore.Audio.AudioColumns.DATE_ADDED;


        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.TITLE, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media._ID};
        Cursor c = context.getContentResolver().query(uri, projection, MediaStore.Audio.Media.DATA + "!=0", null, orderBy+" DESC");

        if (c != null) {
            while (c.moveToNext()) {
                AudioModel audioModel = new AudioModel();
                String path = c.getString(0);
                String name = c.getString(1);
                String album = c.getString(2);
                String artist = c.getString(3);
                String duration = c.getString(4);
                int id = c.getInt(5);
                audioModel.setaName(name);
                audioModel.setaAlbum(album);
                audioModel.setaArtist(artist);
                audioModel.setaPath(path);
                audioModel.setDuration(duration);
                audioModel.setID(id);

                Timber.e(" Album :%s", album);
                Timber.e(" Artist :%s", artist);

                tempAudioList.add(audioModel);
            }
            c.close();
        }

        return tempAudioList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mergeAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mergeAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.menu_search) {
            return true;
        } else if (item.getItemId() == R.id.folder) {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("audio/*");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            }
            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (PICKFILE_RESULT_CODE == requestCode && resultCode == RESULT_OK) {
            if (data.getData() != null) {
                // Retrieve the phone number from the NUMBER column
                File audio_file = new File(Objects.requireNonNull(UriUtils.getPathFromUri(getApplicationContext(), data.getData())));
                AudioModel audioModel = new AudioModel();
                audioModel.setaPath(audio_file.getAbsolutePath());
                audioModel.setaName(audio_file.getName());
              //  audioModelList.add(audioModel);
                startActivity(audio_file.getPath());

            } else if (data.getClipData() != null) {
                if (data.getClipData() != null) {

                    for (int i = 0; i < data.getClipData().getItemCount(); i++) {

                        File audio_file = new File(Objects.requireNonNull(UriUtils.getPathFromUri(getApplicationContext(), data.getClipData().getItemAt(i).getUri())));
                        AudioModel audioModel = new AudioModel();
                        audioModel.setaPath(audio_file.getAbsolutePath());
                        audioModel.setaName(audio_file.getName());
                      //  audioModelList.add(audioModel);
                       startActivity(audio_file.getPath());
                    }
                }
            }
        }else if (requestCode==200){
            new AsyncTaskAllAudio().execute();
        }


    }

    MediaPlayer mediaPlayer;

    private void initMediaPlayer(String path) {
        try {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
            mediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.fromFile(new File(path)));
            mediaPlayer.start();
        } catch (Exception ignore) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void startActivity(String path){
        startActivityForResult(new Intent(getApplicationContext(),EditActivity.class).putExtra("path",path),200);
    }


    public void shareAudioFile(Uri path){
        //FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName(), new File(path)
        try {
            Intent intent =  new Intent()
                    .setAction(Intent.ACTION_SEND)
                    .putExtra(Intent.EXTRA_STREAM, path)
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    .setType("audio/*");
            startActivity(intent);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Could not share this file, I'm aware of the issue.", Toast.LENGTH_SHORT).show();
        }
    }

    private void goToMusicXProPrime(){
        try {
            Uri uriAlter = Uri.parse("http://play.google.com/store/apps/details?id=com.onecupcode.musicxpro.paid");
            Intent intent = new Intent(Intent.ACTION_VIEW,uriAlter);
            if (intent.resolveActivity(getPackageManager())!=null){
                startActivity(intent);
            }
        }catch (ActivityNotFoundException ignore){

        }
    }

}