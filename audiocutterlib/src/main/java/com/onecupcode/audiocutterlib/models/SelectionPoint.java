package com.onecupcode.audiocutterlib.models;

public class SelectionPoint {

    int xposition,yposition;
    double startsec,endsec;

  public   SelectionPoint(int xposition,int yposition, double startsec,double endsec){

        this.xposition = xposition;
        this.yposition = yposition;
        this.startsec = startsec;
        this.endsec = endsec;
    }

    public int getXposition() {
        return xposition;
    }

    public int getYposition() {
        return yposition;
    }

    public double getStartsec() {
        return startsec;
    }

    public double getEndsec() {
        return endsec;
    }
}
