package com.onecupcode.audioeditor.audiocutter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentManager;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.os.Handler;
import android.os.Looper;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.chibde.visualizer.SquareBarVisualizer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.RangeSeekBar;
import com.onecupcode.audioeditor.UriUtils;
import com.onecupcode.audioeditor.dialog.OutputDialogFragment;
import com.onecupcode.audioeditor.miniplayer.MiniPlayer;
import com.onecupcode.audioeditor.videotoaudio.Utils;
import com.onecupcode.audioeditor.videotoaudio.VideoTrimmer;
import com.onecupcode.audioeditor.videotoaudio.view.ProgressBarView;
import com.onecupcode.audioeditor.videotoaudio.view.RangeSeekBarView;
import com.onecupcode.audioeditor.worker.AudioMergeWork;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class EditActivity extends AppCompatActivity implements View.OnClickListener, RangeSeekBar.OnRangeSeekBarChangeListener<Long>, SeekBar.OnSeekBarChangeListener, MediaPlayer.OnCompletionListener, OutputDialogFragment.OutputDialogListener {


    AppCompatSeekBar seekBar;
    AppCompatTextView totalDuration;
    AppCompatTextView progressDuration;
    FloatingActionButton playFlotingActionBtn;

    MediaPlayer mediaPlayer;
    private long startTime = 0;
    private final Handler myHandler = new Handler(Looper.getMainLooper());

    RangeSeekBar<Long> rangeSeekBar;

    AppCompatButton audio_trimmer_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24);



        rangeSeekBar = findViewById(R.id.rangeSeekBar);

        getSupportActionBar().setTitle(""+new File(getIntent().getStringExtra("path")).getName());

        seekBar = findViewById(R.id.seekBar);
        totalDuration = findViewById(R.id.totalDuration);
        progressDuration = findViewById(R.id.durationProgress);
        playFlotingActionBtn = findViewById(R.id.playButton);

        audio_trimmer_btn = findViewById(R.id.trimAudio);
        audio_trimmer_btn.setOnClickListener(this);

        playFlotingActionBtn.setOnClickListener(this);
        playFlotingActionBtn.setOnClickListener(this);




        initMediaPlayer();
    }


    @Override
    public void onClick(View view) {

    /*    Uri newUri =  Uri.parse(getIntent().getStringExtra("path"));// FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, new File(getIntent().getStringExtra("path")));;
        shareAudioFile(newUri);
        */
        if (R.id.playButton==view.getId()){

            if (mediaPlayer!=null){
                playSong();
            }

        }
        if (R.id.trimAudio==view.getId()){
            if ((rangeSeekBar.getSelectedMaxValue()-rangeSeekBar.getSelectedMinValue())<=100){
                Toast.makeText(this, "This is too small to Trim", Toast.LENGTH_SHORT).show();
            }else {
                stopButton();
                showEditDialog();
            }

        }
    }

    private void playSong(){
        if (mediaPlayer.isPlaying()){
            stopButton();
        }else {
            mediaPlayer.start();
            playFlotingActionBtn.setImageResource(R.drawable.ic_baseline_pause_24);
        }
    }


    private void stopButton(){
        mediaPlayer.pause();
        playFlotingActionBtn.setImageResource(R.drawable.ic_baseline_play_arrow_24);
    }


    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void initMediaPlayer(){
        mediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(new File(getIntent().getStringExtra("path")).getAbsolutePath()));

        if (mediaPlayer==null){
            Toast.makeText(getApplicationContext(),"Can't trim this audio",Toast.LENGTH_LONG);
            finish();
        }



        mediaPlayer.setOnCompletionListener(this);

        startTime = mediaPlayer.getCurrentPosition();
        seekBar.setMax( mediaPlayer.getDuration());


        @SuppressLint("DefaultLocale") String tot = String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) mediaPlayer.getDuration()), TimeUnit.MILLISECONDS.toSeconds((long) mediaPlayer.getDuration()) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                        mediaPlayer.getDuration())));

        totalDuration.setText(tot);
        progressDuration.setText(String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes( startTime),
                TimeUnit.MILLISECONDS.toSeconds( startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                toMinutes( startTime))));

        seekBar.setProgress((int)startTime);

        rangeSeekBar.setRangeValues((long)mediaPlayer.getCurrentPosition(),(long)mediaPlayer.getDuration());


        myHandler.postDelayed(UpdateSongTime,100);

        seekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (myHandler!=null){
            myHandler.removeCallbacks(UpdateSongTime);
        }
        if (mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    public void shareAudioFile(Uri path){
        //FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName(), new File(path)
        try {
            Intent intent =  new Intent()
                    .setAction(Intent.ACTION_SEND)
                    .putExtra(Intent.EXTRA_STREAM, path)
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    .setType("audio/*");
            startActivity(intent);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Could not share this file, I'm aware of the issue.", Toast.LENGTH_SHORT).show();
        }
    }

    Runnable UpdateSongTime = new Runnable() {
        @SuppressLint("DefaultLocale")
        public void run() {
            startTime = mediaPlayer.getCurrentPosition();
        /*    if (startTime<rangeSeekBar.getSelectedMinValue() || startTime> rangeSeekBar.getSelectedMaxValue()){
                stopButton();
            }*/
            progressDuration.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes(startTime),
                    TimeUnit.MILLISECONDS.toSeconds(startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)))
            );
            seekBar.setProgress((int)startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    @Override
    public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Long minValue, Long maxValue) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {


            runOnUiThread(()->{
                if (b){
                    mediaPlayer.seekTo(i);
                }
            });

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {


    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        playFlotingActionBtn.setImageResource(R.drawable.ic_baseline_play_arrow_24);
    }
    public void processSelectedFile(String startTime,String durationTime,String inputFileNamePath,String fileName,String format,boolean isBitrate,String bitRate,String sampleRate){

        //ffmpeg -i file.mkv -ss 00:00:20 -to 00:00:40 -c copy file-2.mkv

        List<String> commandList =  new ArrayList<>();

        String extension = getIntent().getStringExtra("path").substring(getIntent().getStringExtra("path").lastIndexOf("."));
        String outputPath = getOutPutFilePath(fileName,""+extension);

        if(new File(outputPath).exists()){
            // createFileNameDialog("Given File name Already Exist, Try new File Name",fileName);
            return;
        }

        commandList.add("-i");
        commandList.add(inputFileNamePath);
        commandList.add("-ss");
        commandList.add(startTime);
        commandList.add("-to");
        commandList.add(durationTime);
        commandList.add("-c");
        commandList.add("copy");
        commandList.add(outputPath);

        String[] command = new String[commandList.size()];
        for (int i=0 ; i < commandList.size() ; i++){
            command[i] = commandList.get(i);
        }
        // ffmpegTestTaskQuit(command);
        WorkRequest mergeWorkRequest = new OneTimeWorkRequest.Builder(AudioMergeWork.class)
                .setInputData(createInputData(command,outputPath,fileName))
                .build();

        WorkManager.getInstance(getApplicationContext())
                .enqueue(mergeWorkRequest);

        showWorkInfoProgress(fileName,outputPath,mergeWorkRequest.getId());

//        startActivity(new Intent(getApplicationContext(),ExampleActivity.class).putExtra("commend",commend.toString()));
    }

    private Data createInputData(String[] command, String outputPath, String fileName){
        Data.Builder builder = new Data.Builder();
        builder.putStringArray("command",command);
        builder.putString("filename",fileName);
        builder.putString("outputpath",outputPath);
        return builder.build();
    }


    private String getOutPutFilePath(String fileName,String format){
        return UriUtils.makeFilename("Audio_Cutter",fileName,format);
    }

    @SuppressLint("SetTextI18n")
    private void showWorkInfoProgress(String fileName, String outputPath, UUID uuid){


        AlertDialog.Builder alerdialog = new AlertDialog.Builder(EditActivity.this);
        alerdialog.setTitle("Audio Trimmer...");
        alerdialog.setMessage(fileName);
        alerdialog.setView(R.layout.progress_dialog);
        alerdialog.setCancelable(false);
        alerdialog.setPositiveButton("Do In Background",(dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        AlertDialog aler = alerdialog.show();

        WorkManager.getInstance(getApplicationContext())
                // requestId is the WorkRequest id
                .getWorkInfoByIdLiveData(uuid)
                .observe(EditActivity.this, workInfo -> {
                    if (workInfo != null) {
                        if (workInfo.getState()== WorkInfo.State.SUCCEEDED){
                            Intent intent = new Intent(EditActivity.this, MiniPlayer.class);
                            intent.putExtra("path",outputPath);
                            startActivity(intent);
                            if (alerdialog!=null){
                                aler.dismiss();
                            }

                        }else if (workInfo.getState() == WorkInfo.State.CANCELLED || workInfo.getState() == WorkInfo.State.FAILED){
                            if (alerdialog!=null){
                                alerdialog.setTitle("Audio Cutter Failed");
                                alerdialog.setMessage("retry");
                            }
                        }
                        // Do something with progress
                    }
                });
    }


    private void showEditDialog() {
        FragmentManager fm = getSupportFragmentManager();
        OutputDialogFragment editNameDialogFragment = OutputDialogFragment.newInstance("cut","Audio Trimmer",""+new File(getIntent().getStringExtra("path")).getName());
        editNameDialogFragment.show(fm, "fragment_edit_name");
        editNameDialogFragment.initOutPutDialogListener(this);
    }

    @Override
    public void onOutPutDialogClosed(String fileName, String format, boolean bitRateFlag, String bitrate, String sampleRate) {
        processSelectedFile(""+Utils.convertSecondsToTime(rangeSeekBar.getSelectedMinValue().intValue()/1000),
                ""+ Utils.convertSecondsToTime(rangeSeekBar.getSelectedMaxValue().intValue()/1000),
                ""+getIntent().getStringExtra("path"),
                ""+fileName,
                ""+format,
                bitRateFlag,
                ""+bitrate,
                ""+sampleRate);
    }

    @Override
    public void onOutPutDialogCanceled() {

    }

}