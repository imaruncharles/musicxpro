package com.onecupcode.audiocutterlib.SoundEditor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.onecupcode.audiocutterlib.R;


public class MarkerView extends RelativeLayout {
    
    private int mVelocity = 0;
    private MarkerListener mListener;


 public    interface MarkerListener {

         void markerTouchStart(MarkerView marker, float pos);

         void markerTouchMove(MarkerView marker, float pos);

         void markerTouchEnd(MarkerView marker);

         void markerFocus(MarkerView marker);

         void markerLeft(MarkerView marker,int velocity);

         void markerRight(MarkerView marker, int velocity);

         void markerEnter(MarkerView marker);

         void markerKeyUp();

         void markerDraw();
    }
    
    public MarkerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MarkerView);
        int rotation = a.getInt(R.styleable.MarkerView_setRotation,0);
        Resources r = getResources();

        ImageView mMarkerImage = new ImageView(context);
        RelativeLayout.LayoutParams mMarkerParent =new  RelativeLayout.LayoutParams(pxtodp(r, 26), pxtodp(r, 26));
        if (rotation==180){
            mMarkerParent.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            mMarkerImage.setImageResource(R.drawable.ic_left_arrow);

        }else {
            mMarkerParent.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            mMarkerImage.setImageResource(R.drawable.ic_right_arrow);
        }


        mMarkerImage.setLayoutParams( mMarkerParent);

        addView(mMarkerImage);
        // Make sure we get keys
        setFocusable(true);

        mVelocity = 0;
        mListener = null;
        a.recycle();
    }

    private int pxtodp(Resources r,int value){

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                (float)value,
                r.getDisplayMetrics());
    }

    public void setListener(MarkerListener listener){
       mListener = listener;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (event.getAction()==MotionEvent.ACTION_DOWN){
            requestFocus();
            mListener.markerTouchStart(this, event.getRawX());

        }else if (event.getAction()==MotionEvent.ACTION_MOVE){
            mListener.markerTouchMove(this, event.getRawX());
        }else if (event.getAction()==MotionEvent.ACTION_UP){
            mListener.markerTouchEnd(this);
        }

        return true;
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, @Nullable Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
       if (gainFocus && mListener !=null){
           mListener.markerFocus(this);
       }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mListener !=null){
            mListener.markerDraw();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        mVelocity = mVelocity + 1;
        int v = (int) Math.sqrt((double) (1+mVelocity)/2);
        if (mListener !=null){
            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                mListener.markerLeft(this, v);
                return true;
            } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                mListener.markerRight(this, v);
                return true;
            } else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                mListener.markerEnter(this);
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        mVelocity = 0;
        if (mListener != null)
            mListener.markerKeyUp();
        return super.onKeyUp(keyCode, event);
    }
}
