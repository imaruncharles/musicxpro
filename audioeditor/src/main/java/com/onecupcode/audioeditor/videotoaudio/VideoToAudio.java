package com.onecupcode.audioeditor.videotoaudio;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SearchView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onecupcode.audioeditor.AudioMerger;
import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.UriUtils;
import com.onecupcode.audioeditor.dialog.OutputDialogFragment;
import com.onecupcode.audioeditor.miniplayer.MiniPlayer;
import com.onecupcode.audioeditor.videotoaudio.interfaces.OnK4LVideoListener;
import com.onecupcode.audioeditor.videotoaudio.interfaces.OnTrimVideoListener;
import com.onecupcode.audioeditor.videotoaudio.model.Video;
import com.onecupcode.audioeditor.worker.AudioMergeWork;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class VideoToAudio extends AppCompatActivity implements VideoAdapter.OnVideoSelectedListener {

    private  final int PICKFILE_RESULT_CODE = 100;
    VideoAdapter videoAdapter;
    List<Video> videoList = new ArrayList<>();
    RecyclerView recyclerView;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_to_audio);

        isStoragePermissionGranted();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.video_to_audio);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24);

        CardView trailView = findViewById(R.id.trail_view); //.from(getApplicationContext()).inflate(R.layout.trail_view,null,false);

        if (getIntent().getStringExtra("build").equals("com.onecupcode.musicxpro.paid")){
            trailView.setVisibility(View.GONE);
        }

        trailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToMusicXProPrime();
            }
        });

        recyclerView = findViewById(R.id.recycler_video);
        videoAdapter = new VideoAdapter(videoList,this);
        recyclerView.setAdapter(videoAdapter);

        getVideoList();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICKFILE_RESULT_CODE && resultCode == Activity.RESULT_OK){
            if (data!=null){
                File audio_file = new File(
                        Objects.requireNonNull(
                                UriUtils.getPathFromUri(
                                        getApplicationContext(), data.getData()
                                )
                        )
                );

                startActivity(new Intent(getApplicationContext(),VideoTrimmer.class).putExtra("path",audio_file.getPath()));

            }
        }


    }

    public void isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        } else {
        }
    }

    Menu menu;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.search_menu,menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                videoAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                videoAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }else
        if (item.getItemId() == R.id.menu_search){
            return true;
        }
        else if (item.getItemId()==R.id.folder){
            selectVideo();
        }

        return super.onOptionsItemSelected(item);

    }

    void selectVideo(){
        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFile.setType("video/*");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        }
        chooseFile = Intent.createChooser(chooseFile, "Choose a file");
        startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);

    }


    @Override
    public void onVideoSelected(Video video) {
        startActivity(new Intent(getApplicationContext(),VideoTrimmer.class).putExtra("path",video.getPath()));
    }

    public void getVideoList() {

        String[] projection = new String[]{
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.SIZE,
                MediaStore.Video.Media.DATA
        };

        Cursor cursor = getApplicationContext().getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                null
        );

        // Cache column indices.
        int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
        int nameColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME);
        int durationColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION);
        int sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE);
        int pathColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);


        if (cursor.moveToFirst()){
            do {
                // Get values of columns for a given video.
                long id = cursor.getLong(idColumn);
                String name = cursor.getString(nameColumn);
                int duration = cursor.getInt(durationColumn);
                int size = cursor.getInt(sizeColumn);
                String path = cursor.getString(pathColumn);

                Uri contentUri = ContentUris.withAppendedId(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI, id);

                // Stores column values and the conten
                // tUri in a local object
                // that represents the media file.
                videoList.add(new Video(id,contentUri, name, duration, size,path));

            }while (cursor.moveToNext());
        }

        videoAdapter.notifyDataSetChanged();
    }

    private void goToMusicXProPrime(){
        try {
            Uri uriAlter = Uri.parse("http://play.google.com/store/apps/details?id=com.onecupcode.musicxpro.paid");
            Intent intent = new Intent(Intent.ACTION_VIEW,uriAlter);
            if (intent.resolveActivity(getPackageManager())!=null){
                startActivity(intent);
            }
        }catch (ActivityNotFoundException ignore){

        }
    }
}