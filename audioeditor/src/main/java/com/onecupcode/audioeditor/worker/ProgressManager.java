package com.onecupcode.audioeditor.worker;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.miniplayer.MiniPlayer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class ProgressManager {
    Context context;
    NotificationManagerCompat notificationManagerCompat;
    int CHANNELID;
    ProgressManager(Context context){
        this.context = context;
        notificationManagerCompat = NotificationManagerCompat.from(context);
        CHANNELID = createID(); //CHANNELID + new Random().nextInt(1000000);
        createNotificationChannel();
    }
    private void createNotificationChannel(){
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = notificationManagerCompat.getNotificationChannel(""+CHANNELID);
            if (notificationChannel==null){
                notificationChannel =new  NotificationChannel(""+CHANNELID,"ForegroundWorker", NotificationManager.IMPORTANCE_HIGH);
                notificationManagerCompat.createNotificationChannel(notificationChannel);
            }
            notificationChannel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            notificationChannel.enableLights(true);
            notificationChannel.enableVibration(true);

        }
    }

    Handler handler = new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context,""+CHANNELID);
            RemoteViews notificationLayout;
            if (msg.getData().getInt("status")==0){
                 notificationLayout = new RemoteViews(context.getPackageName(), R.layout.notificationfinished);
                Intent intent = new Intent(context, MiniPlayer.class);
                intent.putExtra("path",msg.getData().getString("path"));
                int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, uniqueInt, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                notificationBuilder.setContentIntent(pendingIntent);
            }
            else if (msg.getData().getInt("status")==1 || msg.getData().getInt("status")==2){
                notificationLayout = new RemoteViews(context.getPackageName(), R.layout.notificationfinished);
                notificationLayout.setImageViewResource(R.id.progress,R.drawable.ic_baseline_cancel_24);
            }
            else {
                 notificationLayout = new RemoteViews(context.getPackageName(), R.layout.small_notification_view);
            }


            notificationBuilder.setContentTitle("Music XPro Merger ["+msg.getData().getString("filename")+"]");
            notificationBuilder.setContentText(msg.getData().getString("message"));
            notificationBuilder.setStyle(new NotificationCompat.DecoratedCustomViewStyle());
            notificationBuilder.setCustomContentView(notificationLayout);
            notificationBuilder.setCustomBigContentView(notificationLayout);
            notificationLayout.setTextViewText(R.id.text,msg.getData().getString("message"));
            notificationLayout.setTextViewText(R.id.title,"Music XPro Merger ["+msg.getData().getString("filename")+"]");
            //notificationLayout.setProgressBar(R.id.progress);
             notificationBuilder.setSmallIcon(R.drawable.musicgradient1);
             notificationBuilder    .setCategory(NotificationCompat.CATEGORY_SERVICE);
             notificationBuilder    .setPriority(NotificationCompat.PRIORITY_MAX);
             notificationBuilder .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
             notificationBuilder.setOngoing(msg.getData().getBoolean("ongoing"));
             // notificationBuilder.setProgress(100, msg.arg1, true);
             Notification notification = notificationBuilder.build();
             notificationManagerCompat.notify(CHANNELID, notification);
        }
    };

    public void updateProgress(String message,String fileName,boolean ongoing,int status,String path) {
        Message msg = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("message",message);
        bundle.putString("filename",fileName);
        bundle.putBoolean("ongoing",ongoing);
        bundle.putInt("status",status);
        bundle.putString("path",path);
        msg.setData(bundle);
        handler.sendMessageDelayed(msg,100);
    }


    public int createID(){
        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));
        return id;
    }

}


