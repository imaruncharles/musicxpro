package com.onecupcode.audioeditor.miniplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.onecupcode.audioeditor.AudioModel;
import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.audiocutter.ChooseContactActivity;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class MiniPlayer extends AppCompatActivity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, MediaPlayer.OnCompletionListener {

    AppCompatButton editTag;
    AppCompatImageButton shareBtn;
    AppCompatSeekBar seekBar;
    AppCompatTextView totalDuration;
    AppCompatTextView progressDuration;
    AppCompatTextView title;
    FloatingActionButton playFlotingActionBtn;
    AppCompatImageButton closeBtn;

    MediaPlayer mediaPlayer;
    private double startTime = 0;
    private double finalTime = 0;
    private Handler myHandler = new Handler(Looper.getMainLooper());
    public  int oneTimeOnly = 0;


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mini_player);

        title = findViewById(R.id.title);

        closeBtn = findViewById(R.id.closeBtn);
        closeBtn.setOnClickListener(this);

        title.setText(""+new File(getIntent().getStringExtra("path")).getName());

       /* try {
            audioModel = getAudioFromParh();
        }catch (Exception e){
            e.printStackTrace();
        }*/

        editTag = findViewById(R.id.tag_editor);
        shareBtn = findViewById(R.id.shareBtn);
        seekBar = findViewById(R.id.seekBar);
        totalDuration = findViewById(R.id.totalDuration);
        progressDuration = findViewById(R.id.durationProgress);
        playFlotingActionBtn = findViewById(R.id.playButton);

        playFlotingActionBtn.setOnClickListener(this);
        shareBtn.setOnClickListener(this);
        editTag.setOnClickListener(this);
        playFlotingActionBtn.setOnClickListener(this);



        initMediaPlayer();
    }

    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }

    public  boolean checkAndRequestContactsPermissions(Activity activity) {
        int modifyAudioPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS);
        if (modifyAudioPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS},
                    105);
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        if (R.id.shareBtn==view.getId()){
            Uri newUri =  Uri.parse(getIntent().getStringExtra("path"));// FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, new File(getIntent().getStringExtra("path")));;
            shareAudioFile(newUri);
        }else if (R.id.tag_editor==view.getId()){

            if (checkAndRequestContactsPermissions(MiniPlayer.this)){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(MiniPlayer.this)) {
                        openAndroidPermissionsMenu();

                    }else {
                        popupMenu(view);
                    }
                }else {
                        popupMenu(view);
                }
            }

        }else if (R.id.playButton==view.getId()){

            if (mediaPlayer!=null){
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                    playFlotingActionBtn.setImageResource(R.drawable.ic_baseline_play_arrow_24);
                }else {
                    mediaPlayer.start();
                    playFlotingActionBtn.setImageResource(R.drawable.ic_baseline_pause_24);
                }
            }

        }else if (R.id.closeBtn==view.getId()){
            onBackPressed();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer!=null){
            mediaPlayer.pause();
        }
    }




    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void initMediaPlayer(){
        mediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.fromFile(new File(getIntent().getStringExtra("path"))));

        finalTime = mediaPlayer.getDuration();
        startTime = mediaPlayer.getCurrentPosition();

        if (oneTimeOnly == 0) {
            seekBar.setMax((int) finalTime);
            oneTimeOnly = 1;
        }

        @SuppressLint("DefaultLocale") String tot = String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) finalTime), TimeUnit.MILLISECONDS.toSeconds((long) finalTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)
                                finalTime)));

        totalDuration.setText(tot);
        progressDuration.setText(String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                toMinutes((long) startTime))));

        seekBar.setProgress((int)startTime);
        myHandler.postDelayed(UpdateSongTime,100);

        seekBar.setOnSeekBarChangeListener(this);
        mediaPlayer.setOnCompletionListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (myHandler!=null){
            myHandler.removeCallbacks(UpdateSongTime);
        }
        if (mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    public void shareAudioFile(Uri path){
        //FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName(), new File(path)
        try {
            Intent intent =  new Intent()
                    .setAction(Intent.ACTION_SEND)
                    .putExtra(Intent.EXTRA_STREAM, path)
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    .setType("audio/*");
            startActivity(intent);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Could not share this file, I'm aware of the issue.", Toast.LENGTH_SHORT).show();
        }
    }

     Runnable UpdateSongTime = new Runnable() {
        @SuppressLint("DefaultLocale")
        public void run() {
            startTime = mediaPlayer.getCurrentPosition();
            progressDuration.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) startTime),
                    TimeUnit.MILLISECONDS.toSeconds((long) startTime) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                    toMinutes((long) startTime)))
            );
            seekBar.setProgress((int)startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        runOnUiThread(()->{
            if (b){
                mediaPlayer.seekTo(i);

            }
        });
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        playFlotingActionBtn.setImageResource(R.drawable.ic_baseline_play_arrow_24);
    }

    private void popupMenu(View view){
        PopupMenu popupMenu = new PopupMenu(view.getContext(),view);
        popupMenu.inflate(R.menu.audiocutter_menu);
        popupMenu.setOnMenuItemClickListener(menuItem -> {
            if (menuItem.getItemId()==R.id.set_ringtone){
                onMoreMenuSelected(0);
            }else if (menuItem.getItemId() == R.id.assign_to_contact){
                onMoreMenuSelected(1);
            }else if (menuItem.getItemId() == R.id.set_notification){
                onMoreMenuSelected(2);
            }else if (menuItem.getItemId() == R.id.set_alarm){
                onMoreMenuSelected(3);
            }else if (menuItem.getItemId() == R.id.shareAudio){
                onMoreMenuSelected(4);
            }
            return false;
        });

        popupMenu.show();
    }

    public void onMoreMenuSelected(int flagt) {

        Uri uriPath = Uri.parse(new File(getIntent().getStringExtra("path")).getAbsolutePath());


        if (flagt==0){
            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_RINGTONE,uriPath);
            Toast.makeText(
                    MiniPlayer.this,
                    R.string.default_ringtone_success_message,
                    Toast.LENGTH_SHORT)
                    .show();
        }else if (flagt == 1){

            try {
                //Go to the choose contact activity
                Intent intent = new Intent(Intent.ACTION_EDIT, uriPath);
                intent.setClassName(
                        getPackageName(),
                        ChooseContactActivity.class.getName());
                startActivityForResult(intent, 100);
            } catch (Exception e) {
            }

        }else if (flagt == 2){
            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_NOTIFICATION,uriPath);
            Toast.makeText(
                    MiniPlayer.this,
                    R.string.default_notification_success_message,
                    Toast.LENGTH_SHORT)
                    .show();
        }else if (flagt == 3){
            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),RingtoneManager.TYPE_ALARM,uriPath);
            Toast.makeText(
                    MiniPlayer.this,
                    R.string.default_alarm_success,
                    Toast.LENGTH_SHORT)
                    .show();
        } else if (flagt == 4){
           // Uri newUri =  Uri.parse(getIntent().getStringExtra("path"));// FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, new File(getIntent().getStringExtra("path")));;
            shareAudioFile(uriPath);
        }
    }

    private AudioModel getAudioFromParh(){
        AudioModel audioModel = new AudioModel();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.TITLE, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media._ID};
        Cursor c = getApplicationContext().getContentResolver().query(uri, projection, MediaStore.Audio.AudioColumns.DATA + "=?", new String[]{getIntent().getStringExtra("path")}, null);

        if (c!=null){
            if (c.moveToFirst()){
                String path = c.getString(0);
                String name = c.getString(1);
                String album = c.getString(2);
                String artist = c.getString(3);
                String duration = c.getString(4);
                int id = c.getInt(5);
                audioModel.setaName(name);
                audioModel.setaAlbum(album);
                audioModel.setaArtist(artist);
                audioModel.setaPath(path);
                audioModel.setDuration(duration);
                audioModel.setID(id);
            }

            c.close();
        }

        return audioModel;
    }

}