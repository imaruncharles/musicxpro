package com.onecupcode.audioeditor.videotoaudio.model;

import android.net.Uri;

public class Video {
    private long ID;
    private  Uri uri;
    private  String name;
    private  int duration;
    private  int size;
    private  String path;
    public Video(long ID,Uri uri, String name, int duration, int size, String path) {
        this.ID = ID;
        this.uri = uri;
        this.name = name;
        this.duration = duration;
        this.size = size;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public long getID() {
        return ID;
    }

    public Uri getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public int getSize() {
        return size;
    }
}
