package com.onecupcode.audioeditor.videotoaudio;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.videotoaudio.model.Video;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VHolder> implements Filterable {

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
               String charString = charSequence.toString();
               if (charString.isEmpty()){
                   videoModelFilter = videoModel;
               }else {
                   List<Video> filteredList = new ArrayList<>();
                   for (Video video : videoModel){
                       if (video.getName().toLowerCase().contains(charString.toLowerCase())){
                           filteredList.add(video);
                       }
                   }
                   videoModelFilter = filteredList;
               }

               FilterResults filterResults = new FilterResults();
               filterResults.values = videoModelFilter;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                videoModelFilter = (ArrayList<Video>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnVideoSelectedListener{
       void   onVideoSelected(Video video);
    }
    List<Video> videoModel;
    List<Video> videoModelFilter;
    OnVideoSelectedListener onVideoSelectedListener;

    VideoAdapter(List<Video> videos,OnVideoSelectedListener onVideoSelectedListener){

        this.videoModel = videos;
        this.videoModelFilter = videos;
        this.onVideoSelectedListener = onVideoSelectedListener;


    }

    @NonNull
    @Override
    public VHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.video_item,parent,false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull VHolder holder, int position) {

        holder.videoImg.post(() -> {
            try {
                BitmapFactory.Options options=new BitmapFactory.Options();
                Bitmap thumbnail =  MediaStore.Video.Thumbnails.getThumbnail(holder.videoImg.getContext().getContentResolver(),videoModelFilter.get(position).getID(),MediaStore.Video.Thumbnails.MINI_KIND,options);
                holder.videoImg.setImageBitmap(thumbnail);
            }catch (Exception e){
                e.printStackTrace();
            }

        });

        holder.videoName.setText(""+videoModelFilter.get(position).getName());
        holder.duration.setText(""+Utils.convertSecondsToTime(videoModelFilter.get(position).getDuration()/1000));
    }


    @Override
    public int getItemCount() {
        return videoModelFilter.size();
    }

    class VHolder extends RecyclerView.ViewHolder{

        AppCompatTextView videoName;
        AppCompatImageView videoImg;
        AppCompatTextView duration;
        public VHolder(@NonNull View itemView) {
            super(itemView);

            videoImg = itemView.findViewById(R.id.videoImg);
            videoName = itemView.findViewById(R.id.videoName);
            duration = itemView.findViewById(R.id.duration);
            itemView.setOnClickListener(view -> onVideoSelectedListener.onVideoSelected(videoModelFilter.get(getAdapterPosition())));
        }
    }
}
