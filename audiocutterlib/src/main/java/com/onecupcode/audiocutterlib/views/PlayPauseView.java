package com.onecupcode.audiocutterlib.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.onecupcode.audiocutterlib.R;

public class PlayPauseView extends FrameLayout {


    private static long PLAY_PAUSE_ANIMATION_DURATION = 400;
    private static Property<PlayPauseView, Integer> COLOR = new Property<PlayPauseView, Integer>(Integer.class,"color") {
        @Override
        public Integer get(PlayPauseView playPauseView) {
            return playPauseView.color;
        }

        @Override
        public void set(PlayPauseView object, Integer value) {
            super.set(object, value);
        }
    };

    private PlayPauseDrawable mDrawable;
    private Paint mPaint = new Paint();


    private AnimatorSet mAnimatorSet = null;
    private int mBackgroundColor = 0;
    private int mArrowColor = 0;
    private int mWidth = 0;
    private int mHeight = 0;
    private boolean isPlaying = false;
    private boolean isInteractive = false;
    private float width = 0.0f;
    private float height = 0.0f;
    private float distance = 0.f;
    private int color;

    private int getColor() {
        return mBackgroundColor;
    }

    private void setColor(int color) {
        mBackgroundColor = color;
    }


    public PlayPauseView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);


        TypedArray typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.PlayPauseView,
                0, 0);
        //Reading values from the XML layout
        try {
            mBackgroundColor = typedArray.getColor(R.styleable.PlayPauseView_backgroundcolor, getResources().getColor(R.color.colorPrimary));
            mArrowColor = typedArray.getColor(R.styleable.PlayPauseView_PlayPauseArrow_color, ContextCompat.getColor(context, R.color.colorPrimary));
            width = typedArray.getDimension(R.styleable.PlayPauseView_PlayPauseView_width, 0f);
            height = typedArray.getDimension(R.styleable.PlayPauseView_PlayPauseView_height, 0f);
            distance = typedArray.getDimension(R.styleable.PlayPauseView_PlayPauseView_distance, 0f);
        } finally {
            typedArray.recycle();
        }

        setWillNotDraw(false);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mDrawable = new PlayPauseDrawable(context, width, height, distance, mArrowColor);
        mDrawable.setCallback(this);

    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mDrawable.setBounds(0, 0, w, h);
        mWidth = w;
        mHeight = h;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewOutlineProvider outlineProvider = new ViewOutlineProvider() {

                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setOval(0, 0, view.getWidth(), view.getHeight());

                }
            };
            setOutlineProvider(outlineProvider);
            setClipToOutline( true);
        }
    }

    @Override
    protected boolean verifyDrawable(@NonNull Drawable who) {

        return who == mDrawable || super.verifyDrawable(who);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPaint.setColor(mBackgroundColor);
        float radius = Math.min(mWidth, mHeight) / 2f;
        canvas.drawCircle(mWidth / 2f, mHeight / 2f, radius, mPaint);
        mDrawable.draw(canvas);
    }

   public void toggle() {
        if (mAnimatorSet != null) {
            mAnimatorSet.cancel();
        }




        mAnimatorSet = new AnimatorSet();
        //  final ObjectAnimator colorAnim = ObjectAnimator.ofInt(this, COLOR, isPlay ? mPauseBackgroundColor : mPlayBackgroundColor);
        //  colorAnim.setEvaluator(new ArgbEvaluator());
        Animator pausePlayAnim = mDrawable.getPausePlayAnimator();
        mAnimatorSet.setInterpolator(new DecelerateInterpolator());
        mAnimatorSet.setDuration( PLAY_PAUSE_ANIMATION_DURATION);
        mAnimatorSet.play(pausePlayAnim);
        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                setEnabled(true);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        mAnimatorSet.start();
    }

    public void setPlaying(boolean isPlaying){
       this.isPlaying = isPlaying;
        toggle();
    }

  public   boolean isPlaying(){
        isPlaying = mDrawable.isPlay();
        return isPlaying;
    }



}
