package com.onecupcode.audioeditor.statusmaker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.onecupcode.audioeditor.R;

import java.util.Objects;


public class StatusMaker extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_maker);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Status Maker");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_back_ios_24);
    }


}