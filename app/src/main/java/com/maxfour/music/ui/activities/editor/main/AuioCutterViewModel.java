package com.maxfour.music.ui.activities.editor.main;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.maxfour.music.model.Song;

import java.util.ArrayList;
import java.util.List;

public class AuioCutterViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    MutableLiveData<List<Song>> mutableLiveData = new MutableLiveData<>();

    Runnable runnable;

    Handler  handler   = new android.os.Handler(Looper.myLooper());




    public void getMutableLiveData(Context context, String folderPath){

        List<Song> songList = new ArrayList<>();

          final String[] BASE_PROJECTION = new String[]{
                BaseColumns._ID,// 0
                MediaStore.Audio.AudioColumns.TITLE,// 1
                MediaStore.Audio.AudioColumns.TRACK,// 2
                MediaStore.Audio.AudioColumns.YEAR,// 3
                MediaStore.Audio.AudioColumns.DURATION,// 4
                MediaStore.Audio.AudioColumns.DATA,// 5
                MediaStore.Audio.AudioColumns.DATE_MODIFIED,// 6
                MediaStore.Audio.AudioColumns.ALBUM_ID,// 7
                MediaStore.Audio.AudioColumns.ALBUM,// 8
                MediaStore.Audio.AudioColumns.ARTIST_ID,// 9
                MediaStore.Audio.AudioColumns.ARTIST,// 10
        };

        String selection = MediaStore.Audio.Media.DATA + " LIKE ? AND " + MediaStore.Audio.Media.DATA + " NOT LIKE ? ";
        String[] selectionArgs = new String[]{
                "%" + folderPath + "%",
                "%" + folderPath + "/%/%"
        };



        runnable = () -> {
            Cursor cursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,BASE_PROJECTION,selection,selectionArgs,null);

            if (cursor.moveToFirst()){
                do {

                    Song song = new Song(
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.TRACK)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.YEAR)),
                            cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)),
                            cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DATE_MODIFIED)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)),
                            cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST_ID)),
                            cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                    );

                    //File f = new File(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
                    // if (f.isFile() && f.exists()) {
                    songList.add(song);

                }while (cursor.moveToNext());
            }

            if (cursor != null){
                cursor.close();
            }

            mutableLiveData.setValue(songList);
        };


      handler.post(runnable);



    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (runnable!=null){
            handler.removeCallbacks(runnable);
        }
    }
}