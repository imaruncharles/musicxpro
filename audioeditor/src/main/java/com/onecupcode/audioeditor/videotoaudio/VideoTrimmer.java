package com.onecupcode.audioeditor.videotoaudio;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.onecupcode.audioeditor.R;
import com.onecupcode.audioeditor.UriUtils;
import com.onecupcode.audioeditor.dialog.OutputDialogFragment;
import com.onecupcode.audioeditor.miniplayer.MiniPlayer;
import com.onecupcode.audioeditor.videotoaudio.interfaces.OnK4LVideoListener;
import com.onecupcode.audioeditor.videotoaudio.interfaces.OnTrimVideoListener;
import com.onecupcode.audioeditor.worker.AudioMergeWork;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VideoTrimmer extends AppCompatActivity implements OnTrimVideoListener, OnK4LVideoListener, OutputDialogFragment.OutputDialogListener {
    K4LVideoTrimmer mVideoTrimmer;
    ProgressDialog mProgressDialog;
    String inputFileName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_trimmer);

        initVideo(getIntent().getStringExtra("path"));
    }

    @Override
    public void onTrimStarted() {

    }

    @Override
    public void getResult(Uri uri) {

    }

    @Override
    public void cancelAction() {
        mProgressDialog.cancel();
        mVideoTrimmer.destroy();
        finish();
    }

    @Override
    public void onError(String message) {
        mProgressDialog.cancel();

        runOnUiThread(()->{
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        });

    }

    String startTime="";
    String durationTime ="";
    @Override
    public void getVidoInfoDetails(int startTime, int endTime) {
        this.startTime = Utils.convertSecondsToTime(startTime / 1000);
        this.durationTime = Utils.convertSecondsToTime((endTime - startTime) / 1000);
        showEditDialog();
    }

    @Override
    public void onVideoPrepared(int mDuration) {

        runOnUiThread(()->{
            Toast.makeText(this, "onVideoPrepared", Toast.LENGTH_SHORT).show();
        });
    }

    private void initVideo(String path){

        inputFileName = path;

        mProgressDialog = new ProgressDialog(VideoTrimmer.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(getString(R.string.trimming_progress));
        mVideoTrimmer = findViewById(R.id.timeLine);

        if (mVideoTrimmer != null) {
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnK4LVideoListener(this);
            mVideoTrimmer.setUpOnPreparedListener();
            mVideoTrimmer.setVideoInformationVisibility(true);
            mVideoTrimmer.setVideoURI(Uri.parse(path));
            // mVideoTrimmer.setMaxDuration(600);
        }
    }


    public void processSelectedFile(String startTime,String durationTime,String inputFileNamePath,String fileName,String format,boolean isBitrate,String bitRate,String sampleRate){

        // #$ ffmpeg -i input.mp4 -vn -ar 44100 -ac 2 -ab 320 -f mp3 output.mp3
        //  #ffmpeg -i sample.avi -ss 00:03:05 -t 00:00:45.0 -q:a 0 -map a sample.mp3

        List<String> commandList =  new ArrayList<>();
        String outputPath = getOutPutFilePath(fileName,format);

        if(new File(outputPath).exists()){
            // createFileNameDialog("Given File name Already Exist, Try new File Name",fileName);
            return;
        }

        commandList.add("-i");
        commandList.add(inputFileNamePath);
        commandList.add("-ss");
        commandList.add(startTime);
        commandList.add("-t");
        commandList.add(durationTime);
        commandList.add("-vn");
        if (isBitrate){
            commandList.add("-ar");
            commandList.add(bitRate);
            commandList.add("-b:a");
            commandList.add(sampleRate);
        }
        commandList.add("-f");
        commandList.add(format);
        commandList.add(outputPath);

        String[] command = new String[commandList.size()];
        for (int i=0 ; i < commandList.size() ; i++){
            command[i] = commandList.get(i);
        }
        // ffmpegTestTaskQuit(command);
        WorkRequest mergeWorkRequest = new OneTimeWorkRequest.Builder(AudioMergeWork.class)
                .setInputData(createInputData(command,outputPath,fileName))
                .build();

        WorkManager.getInstance(getApplicationContext())
                .enqueue(mergeWorkRequest);

        showWorkInfoProgress(fileName,outputPath,mergeWorkRequest.getId());

//        startActivity(new Intent(getApplicationContext(),ExampleActivity.class).putExtra("commend",commend.toString()));
    }

    private Data createInputData(String[] command, String outputPath, String fileName){
        Data.Builder builder = new Data.Builder();
        builder.putStringArray("command",command);
        builder.putString("filename",fileName);
        builder.putString("outputpath",outputPath);
        return builder.build();


    }


    private String getOutPutFilePath(String fileName,String format){
        return UriUtils.makeFilename("Video_to_audio",fileName,"."+format);
    }

    @SuppressLint("SetTextI18n")
    private void showWorkInfoProgress(String fileName, String outputPath, UUID uuid){


        AlertDialog.Builder alerdialog = new AlertDialog.Builder(VideoTrimmer.this);
        alerdialog.setTitle("Extracting Audio...");
        alerdialog.setMessage(fileName);
        alerdialog.setView(R.layout.progress_dialog);
        alerdialog.setCancelable(false);
        alerdialog.setPositiveButton("Do In Background",(dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        AlertDialog aler = alerdialog.show();

        WorkManager.getInstance(getApplicationContext())
                // requestId is the WorkRequest id
                .getWorkInfoByIdLiveData(uuid)
                .observe(VideoTrimmer.this, workInfo -> {
                    if (workInfo != null) {
                        if (workInfo.getState()== WorkInfo.State.SUCCEEDED){
                            Intent intent = new Intent(VideoTrimmer.this, MiniPlayer.class);
                            intent.putExtra("path",outputPath);
                            startActivity(intent);
                            if (alerdialog!=null){
                                aler.dismiss();
                            }

                        }else if (workInfo.getState() == WorkInfo.State.CANCELLED || workInfo.getState() == WorkInfo.State.FAILED){
                            if (alerdialog!=null){
                                alerdialog.setTitle("Video to Audio Failed");
                                alerdialog.setMessage("retry");
                            }
                        }
                        // Do something with progress
                    }
                });
    }


    private void showEditDialog() {
        FragmentManager fm = getSupportFragmentManager();
        OutputDialogFragment editNameDialogFragment = OutputDialogFragment.newInstance("","Extract Audio",""+new File(inputFileName).getName());
        editNameDialogFragment.show(fm, "fragment_edit_name");
        editNameDialogFragment.initOutPutDialogListener(this);
    }

    @Override
    public void onOutPutDialogClosed(String fileName, String format, boolean bitRateFlag, String bitrate, String sampleRate) {
        processSelectedFile(""+startTime,
                ""+durationTime,
                ""+inputFileName,
                ""+fileName,
                ""+format,
                bitRateFlag,
                ""+bitrate,
                ""+sampleRate);
    }

    @Override
    public void onOutPutDialogCanceled() {

    }

}