package com.onecupcode.audioeditor;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class AudioModel implements Parcelable , Comparable<AudioModel> {

    int ID;
    String aPath;
    String aName;
    String aAlbum;
    String aArtist;
    String duration;

    public static final Creator<AudioModel> CREATOR = new Creator<AudioModel>() {
        @Override
        public AudioModel createFromParcel(Parcel in) {
            return new AudioModel(in);
        }

        @Override
        public AudioModel[] newArray(int size) {
            return new AudioModel[size];
        }
    };

    public AudioModel() {

    }

    public int getID(){
        return ID;
    }
    public void setID(int ID){
        this.ID = ID;
    }

    public String getaPath() {
        return aPath;
    }
    public void setaPath(String aPath) {
        this.aPath = aPath;
    }
    public String getaName() {
        return aName;
    }
    public void setaName(String aName) {
        this.aName = aName;
    }
    public String getaAlbum() {
        return aAlbum;
    }
    public void setaAlbum(String aAlbum) {
        this.aAlbum = aAlbum;
    }
    public String getaArtist() {
        return aArtist;
    }
    public void setaArtist(String aArtist) {
        this.aArtist = aArtist;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }





    AudioModel(Parcel in){
        ID = in.readInt();
        aPath = in.readString();
        aName = in.readString();
        aAlbum = in.readString();
        aArtist = in.readString();
        duration = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ID);
        parcel.writeString(aPath);
        parcel.writeString(aName);
        parcel.writeString(aAlbum);
        parcel.writeString(aArtist);
        parcel.writeString(duration);

    }

    @Override
    public int compareTo(AudioModel audioModel) {

        if (audioModel == null) return -1;

        if (ID == audioModel.ID)
        {
            return 0;
        }

        return 0;
    }
}



