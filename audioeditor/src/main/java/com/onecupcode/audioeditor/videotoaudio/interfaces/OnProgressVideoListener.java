package com.onecupcode.audioeditor.videotoaudio.interfaces;

public interface OnProgressVideoListener {

    void updateProgress(int time, int max, float scale);
}
