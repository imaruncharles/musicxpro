package com.onecupcode.audiocutterlib;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.onecupcode.audiocutterlib.SoundEditor.CheapSoundFile;
import com.onecupcode.audiocutterlib.SoundEditor.MarkerView;
import com.onecupcode.audiocutterlib.SoundEditor.WaveformView;
import com.onecupcode.audiocutterlib.utils.PermissionManager;
import com.onecupcode.audiocutterlib.utils.Pixels;
import com.onecupcode.audiocutterlib.utils.ViewUtil;
import com.onecupcode.audiocutterlib.views.PlayPauseView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;

import de.mateware.snacky.Snacky;
import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

import static com.onecupcode.audiocutterlib.utils.Constants.ALARM_KEY;
import static com.onecupcode.audiocutterlib.utils.Constants.NOTIFICATION_KEY;
import static com.onecupcode.audiocutterlib.utils.Constants.RINGTONE_KEY;

public class EditAudio extends AppCompatActivity implements View.OnClickListener ,WaveformView.WaveformListener, MarkerView.MarkerListener {
    private final String[] Supported_Format = {".aac", ".AMR", ".mp3", ".wav",".m4a"};
    private int mNewFileKind = 0;
    private int mMarkerLeftInset = 0;
    Thread mSaveSoundFileThread;

    private  int mMarkerRightInset = 0;
    private long mLoadingLastUpdateTime = 0;
    private int mMarkerTopOffset =0;
    private int mMarkerBottomOffset = 0;
    private boolean mLoadingKeepGoing = false;
    private ProgressDialog mProgressDialog = null;
    private CheapSoundFile mSoundFile = null;
    private File mFile;
    private String mFilename;
    private WaveformView mWaveformView  = null;
    private MarkerView mStartMarker = null;
    private   MarkerView mEndMarker = null;
    private  AppCompatTextView mStartText = null;
    private AppCompatTextView mEndText  = null;
    private boolean mKeyDown = false;
    private int mWidth = 0;
    private int mMaxPos = 0;
    private int mStartPos = -1;
    private  int mEndPos = -1;
    private  boolean mStartVisible = false;
    private boolean mEndVisible = false;
    private  int mLastDisplayedStartPos = 0;
    private  int mLastDisplayedEndPos = 0;
    private  int mOffset = 0;
    private  int mOffsetGoal = 0;
    private  int mFlingVelocity = 0;
    private  int mPlayStartMsec = 0;
    private  int mPlayStartOffset = 0;
    private  int mPlayEndMsec = 0;
    private Handler mHandler = null;
    private  boolean mIsPlaying = false;
    private MediaPlayer mPlayer = null;
    private boolean mTouchDragging = false;
    private float  mTouchStart = 0.0f;
    private int  mTouchInitialOffset = 0;
    private int mTouchInitialStartPos = 0;
    private int mTouchInitialEndPos  = 0;
    private long mWaveformTouchStartMsec =  0;
    private float mDensity = 0.0f;
    private File outputFile;
    private int marginvalue  = 0;
    private boolean EdgeReached = false;
    private int mSoundDuration = 0;
    private boolean Maskhidden = true;

    private final Runnable mTimerRunnable = new Runnable(){
        @Override
        public void run() {
            // Updating an EditText is slow on Android.  Make sure
            // we only do the update if the text has actually changed.
            if (mStartPos != mLastDisplayedStartPos && !mStartText.hasFocus()) {
                mStartText.setText(getTimeFormat(formatTime(mStartPos)));
                mLastDisplayedStartPos = mStartPos;
            }

            if (mEndPos != mLastDisplayedEndPos && !mEndText.hasFocus()) {
                mEndText.setText(getTimeFormat(formatTime(mEndPos)));
                mLastDisplayedEndPos = mEndPos;
            }
            mHandler.postDelayed(this, 100);
        }
    };
    
    TextView zoom_in,zoom_out,Editor_song_title;
    AppCompatButton Button_Done;
    ImageView image_Cancel;
    LinearLayout Editor_Save,Editor_Alarm,Editor_Ringtone,Editor_Contacts,Editor_Share;
    RelativeLayout editor_container;
    LinearLayout Editor_Notification;
    PlayPauseView Play_Pause_View;
    
    AppCompatTextView mark_start,mark_end;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().getDecorView().setBackgroundColor(ContextCompat.getColor(this,R.color.app_decorview_color));
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_editor);
   
   
        zoom_in = findViewById(R.id.zoom_in);
        zoom_out = findViewById(R.id.zoom_out);
        Button_Done = findViewById(R.id.Button_Done);
        image_Cancel = findViewById(R.id.image_Cancel);
        Editor_Save = findViewById(R.id.Editor_Save);
        Editor_Alarm = findViewById(R.id.Editor_Alarm);
        Editor_Ringtone = findViewById(R.id.Editor_Ringtone);
        Editor_Contacts = findViewById(R.id.Editor_Contacts);
        Editor_Notification = findViewById(R.id.Editor_Notification);
        Play_Pause_View = findViewById(R.id.Play_Pause_View);
        Editor_song_title = findViewById(R.id.Editor_song_title);
        editor_container = findViewById(R.id.editor_container);
        Editor_Share = findViewById(R.id.Editor_Share);
        
        mark_start = findViewById(R.id.mark_start);
        mark_end = findViewById(R.id.mark_end);

        zoom_in.setOnClickListener(this);
        zoom_out.setOnClickListener(this);
        Button_Done.setOnClickListener(this);
        image_Cancel.setOnClickListener(this);
        Editor_Save.setOnClickListener(this);
        Editor_Notification.setOnClickListener(this);
        Editor_Alarm.setOnClickListener(this);
        Editor_Ringtone.setOnClickListener(this);
        Editor_Contacts.setOnClickListener(this);
        Editor_Share.setOnClickListener(this);

        ViewUtil.SetOntouchListener(Button_Done);
        ViewUtil.SetOntouchListener(Editor_Save);
        ViewUtil.SetOntouchListener(Editor_Notification);
        ViewUtil.SetOntouchListener(Editor_Alarm);
        ViewUtil.SetOntouchListener(Editor_Ringtone);
        ViewUtil.SetOntouchListener(Editor_Contacts);
        ViewUtil.SetOntouchListener(Editor_Share);

        Play_Pause_View.setVisibility(View.INVISIBLE);
        Play_Pause_View.setPlaying(!mIsPlaying);
        Play_Pause_View.setOnClickListener(this);


        Play_Pause_View.postDelayed(() -> runOnUiThread(() -> Play_Pause_View.setVisibility(View.VISIBLE)),400);

        loadGui();



        marginvalue = Pixels.pxtodp(this, 12);
        mPlayer = null;
        mIsPlaying = false;
        mSoundFile = null;
        mKeyDown = false;
        mHandler = new Handler();
        mHandler.postDelayed(mTimerRunnable, 100);

        Bundle extras = getIntent().getExtras();
        String path = extras.getString(KEY_SOUND_COLUMN_path, null);
        String title = extras.getString(KEY_SOUND_COLUMN_title, "");

        if (path != null) {

            // remove mp3 part
            String newtitle;
            if (title.contains(EXTENSION_MP3))
                newtitle = title.replace(EXTENSION_MP3, "") ;
            else
                newtitle = title.toString();
            Editor_song_title.setText( newtitle);
            mFilename = path;

            if (mSoundFile == null) 
                loadFromFile() ;
            else 
                mHandler.post(this::finishOpeningSoundFile);
        }

    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        if (!Maskhidden){
            runanimation();
        }else if (mFilename != null && !mFilename.isEmpty()){
            showExitOptionsDialog();
        }else {
            finish();
        }


    }


    private void showExitOptionsDialog() {

        CharSequence[] colors = {getString(R.string.editor_back_dialog_discard), getString(R.string.editor_back_dialog_cancel)};
        AlertDialog.Builder builder = new  AlertDialog.Builder(this);
        builder.setTitle(R.string.editor_back_dialog_title);
        builder.setItems(colors,(dialogInterface, i) -> {
            if (i==0){
                FinishActivity();
            }
        });

        builder.show();
    }

    private void FinishActivity() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
        }
        mWaveformView.setPlayback(-1);
        mIsPlaying = false;
        finish();
    }

    @Override
    protected void  onDestroy() {
        super.onDestroy();

        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
        mProgressDialog = null;

        finish();

        mSoundFile = null;
        mWaveformView = null;
    }

    @Override
   public void  waveformDraw() {
        if (mWaveformView != null) {
            mWidth = mWaveformView.getMeasuredWidth();
        }
        if (mOffsetGoal != mOffset && !mKeyDown && !EdgeReached) {
            updateDisplay();
        } else if (mIsPlaying) {

            updateDisplay();
        } else if (mFlingVelocity != 0) {

            updateDisplay();
        }
    }

    @Override
    public void waveformTouchStart(float x) {

        mTouchDragging = true;
        mTouchStart = x;
        mTouchInitialOffset = mOffset;
        mFlingVelocity = 0;
        mWaveformTouchStartMsec = System.currentTimeMillis();
    }

    @Override
    public void waveformTouchMove(float x) {
        mOffset = trap((int) (mTouchInitialOffset + (mTouchStart - x)));
        updateDisplay();
    }

    @Override
    public void waveformTouchEnd() {
        mTouchDragging = false;
        mOffsetGoal = mOffset;

        long elapsedMsec = System.currentTimeMillis() - mWaveformTouchStartMsec;
        if (elapsedMsec < 300) {
            if (mIsPlaying) {
                int seekMsec = mWaveformView.pixelsToMillisecs((int) (mTouchStart + mOffset));
                if (seekMsec >= mPlayStartMsec && seekMsec < mPlayEndMsec) {
                    mPlayer.seekTo(seekMsec - mPlayStartOffset);
                } else {
                    handlePause();
                }
            } else {
                onPlay((int)(mTouchStart + mOffset));
            }
        }
    }

    @Override
    public void  waveformFling(float vx) {
        mTouchDragging = false;
        mOffsetGoal = mOffset;
        mFlingVelocity = (int) (-vx);
        updateDisplay();
    }

    @Override
    public void waveformZoomIn() {

        if (mWaveformView.canZoomOut()) {

            marginvalue = Pixels.pxtodp(this, 12);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(Pixels.pxtodp(EditAudio.this, 12), 0, Pixels.pxtodp(EditAudio.this, 12), Pixels.pxtodp(EditAudio.this, 20));
            mWaveformView.setLayoutParams(params);

        }
        mWaveformView.zoomIn();

        mStartPos = mWaveformView.getStart();
                mEndPos = mWaveformView.getEnd();
                mMaxPos = mWaveformView.maxPos();
        mOffset = mWaveformView.getOffset();
                mOffsetGoal = mOffset;
        updateDisplay();


    }


    @Override
    public void waveformZoomOut() {

        if (!mWaveformView.canZoomOut()) {
            marginvalue = Pixels.pxtodp(this, 12);
            FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(Pixels.pxtodp(EditAudio.this, 12), 0, Pixels.pxtodp(EditAudio.this, 12), Pixels.pxtodp(EditAudio.this, 20));
            mWaveformView.setLayoutParams(params); 

        }
        mWaveformView.zoomOut();
        mStartPos = mWaveformView.getStart();
                mEndPos = mWaveformView.getEnd();
                mMaxPos = mWaveformView.maxPos();
        mOffset = mWaveformView.getOffset();
                mOffsetGoal = mOffset;
        updateDisplay();
    }

    //
    // MarkerListener
    //
   @Override
    public void markerDraw() {}

    @Override
    public void markerTouchStart(MarkerView marker, float pos) {
        mTouchDragging = true;
        mTouchStart = pos;
        mTouchInitialStartPos = mStartPos;
        mTouchInitialEndPos = mEndPos;
    }

    @Override
    public void markerTouchMove(MarkerView marker, float pos) {
        float delta = pos - mTouchStart;

        if (marker == mStartMarker) {
            mStartPos = trap((int)(mTouchInitialStartPos + delta));
            if (mStartPos + mStartMarker.getWidth() >= mEndPos) {
                mStartPos = mEndPos - mStartMarker.getWidth();
            }

        } else {
            mEndPos = trap((int)(mTouchInitialEndPos + delta));
            if (mEndPos < mStartPos + mStartMarker.getWidth())
            mEndPos = mStartPos + mStartMarker.getWidth();
        }


        updateDisplay()  ;
    }

    @Override
    public void markerTouchEnd(MarkerView marker) {
        mTouchDragging = false;
        if (marker == mStartMarker) {
            setOffsetGoalStart();
        } else {
            setOffsetGoalEnd();
        }
    }

    @Override
    public void markerLeft(MarkerView marker, int velocity) {
        mKeyDown = true;

        if (marker == mStartMarker) {
            int saveStart = mStartPos;
            mStartPos = trap(mStartPos - velocity);
            mEndPos = trap(mEndPos - (saveStart - mStartPos));
            setOffsetGoalStart();
        }

        if (marker == mEndMarker) {
            if (mEndPos == mStartPos) {
                mStartPos = trap(mStartPos - velocity);
                mEndPos = mStartPos;
            } else {
                mEndPos = trap(mEndPos - velocity);
            }

            setOffsetGoalEnd();
        }

        updateDisplay();
    }

    @Override
    public void markerRight(MarkerView marker, int velocity) {
        mKeyDown = true;

        if (marker == mStartMarker) {
            int saveStart = mStartPos;
            mStartPos += velocity;
            if (mStartPos > mMaxPos)
                mStartPos = mMaxPos;
            mEndPos += mStartPos - saveStart;
            if (mEndPos > mMaxPos)
                mEndPos = mMaxPos;

            setOffsetGoalStart();
        }

        if (marker == mEndMarker) {
            mEndPos += velocity;
            if (mEndPos > mMaxPos)
                mEndPos = mMaxPos;

            setOffsetGoalEnd();
        }

        updateDisplay();
    }

    @Override
    public void markerEnter(MarkerView marker) {

    }

    @Override
    public void markerKeyUp() {
        mKeyDown = false;
        updateDisplay();
    }

    @Override
    public void markerFocus(MarkerView marker) {
        mKeyDown = false;
        if (marker == mStartMarker) {
            setOffsetGoalStartNoUpdate();
        } else {
            setOffsetGoalEndNoUpdate();
        }

        // Delay updaing the display because if this focus was in
        // response to a touch event, we want to receive the touch
        // event too before updating the display.
        mHandler.postDelayed(()->{ this.updateDisplay() ;}, 100);
    }


    //
    // Internal methods
    //

    private void loadGui() {

        
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mDensity = metrics.density;

        mMarkerLeftInset = (int) (13 * mDensity);
        mMarkerRightInset = (int) (13 * mDensity);

        mMarkerTopOffset = (int)(10 * mDensity);
        mMarkerBottomOffset = (int)(10 * mDensity);


        mStartText = findViewById(R.id.starttext);
        mEndText = findViewById(R.id.endtext);
        mark_start.setOnClickListener(this);
        mark_end.setOnClickListener(this);

        enableDisableButtons();

        mWaveformView = findViewById(R.id.waveform);
        mWaveformView.setListener(this);
        mMaxPos = 0;
        mLastDisplayedStartPos = -1;
        mLastDisplayedEndPos = -1;

        if (mSoundFile != null && !mWaveformView.hasSoundFile()) {
            mWaveformView.setSoundFile(mSoundFile);
            mWaveformView.recomputeHeights(mDensity);
            mMaxPos = mWaveformView.maxPos();
        }

        mStartMarker = findViewById(R.id.startmarker);
        mStartMarker.setListener(this);
        mStartMarker.setAlpha(1f);
        mStartMarker.setFocusable( true);
        mStartMarker.setFocusableInTouchMode(true); 
        mStartVisible = true;

        mEndMarker = findViewById(R.id.endmarker);
        mEndMarker.setListener(this);
        mEndMarker.setAlpha(1f);
        mEndMarker.setFocusable( true);
        mEndMarker.setFocusableInTouchMode(true);
        mEndVisible = true;

        updateDisplay();


    }




    private void loadFromFile() {


        mFile = new  File(mFilename);
        String mFileName = mFile.getName();

        boolean FileSupported = false;
        for (String aSupported_Format : Supported_Format) 
            if (mFileName.toLowerCase().contains(aSupported_Format)) {
            FileSupported = true;
            break; }

        if (!FileSupported) {

            new AlertDialog.Builder(EditAudio.this)
                    .setTitle("Alert")
                    .setMessage("Unsupported Format")
                    .setPositiveButton("ok",(dialogInterface, i) -> {
                        finish();
                    })
                    .show();

            return;
        }



        mLoadingLastUpdateTime = System.currentTimeMillis();
        mLoadingKeepGoing = true;


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setTitle(getString(R.string.edit_loading_text));
        mProgressDialog.setOnCancelListener(dialogInterface -> mLoadingKeepGoing = false);

        runOnUiThread(()-> mProgressDialog.show());


        CheapSoundFile.ProgressListener listener = fractionComplete -> {

            long now = System.currentTimeMillis();
            if (now - mLoadingLastUpdateTime > 100) {
                mProgressDialog.setProgress((mProgressDialog.getMax() * (int) fractionComplete));
                mLoadingLastUpdateTime = now;
            }
            return mLoadingKeepGoing;
        };
        
        mProgressDialog.setOnDismissListener(dialogInterface -> 
        {
            Log.e(TAG, "loadFromFile: setOnDismissListener ");
            mEndMarker.setVisibility( View.VISIBLE);
            mStartMarker.setVisibility(View.VISIBLE);
        });

  
        new Thread(){
            @Override
            public void run() {
               // super.run();
                
                try {
                    mPlayer = new MediaPlayer();
                    mPlayer.setDataSource(EditAudio.this, Uri.fromFile(mFile));
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer.prepare();
                }catch (IOException ignored){
                    runOnUiThread(() -> {
                        Toast.makeText(EditAudio.this, "Please try to change file name", Toast.LENGTH_LONG).show();
                       new AlertDialog.Builder(EditAudio.this)
                            .setTitle(R.string.editor_error)
                                .setMessage(R.string.editor_error_msg + " File name contains Special Characters Please change file name and try again.")
                               
                               .setPositiveButton("Ok",(dialogInterface, i) -> {
                                 //  pickFile();
                               })
                               .show();
                    });


                    try {
                        String filePath = mFile.getAbsolutePath();
                        File file = new File(filePath);
                        FileInputStream inputStream = new FileInputStream(file);

                        mPlayer = new  MediaPlayer();
                        mPlayer.setDataSource(inputStream.getFD());
                        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mPlayer.prepare();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }.start();


        // Load the sound file in a background thread
        new Thread(){

            @Override
            public void run() {
             //   super.run();

                try {
                    mSoundFile = CheapSoundFile.create(mFile.getAbsolutePath(), listener);
                } catch (Exception e) {
                    //  Log.e(TAG, "Error while loading sound file" + e);
                    dismissProgessDialog();
                    return;
                }


                if (mLoadingKeepGoing) {

                    mHandler.post(() -> {
                        if (mSoundFile != null) {
                            finishOpeningSoundFile();
                        } else {
                            //Log.e(TAG, "run: editor_error" );
                            dismissProgessDialog();
                           new AlertDialog.Builder(EditAudio.this)
                                .setTitle(R.string.editor_error)
                                    .setMessage(R.string.editor_error_msg)
                                    .setPositiveButton(android.R.string.yes,(dialogInterface, i) -> {
                                       // pickFile();
                                    }).show();


                        }
                    });
                }
                
            }
        }.start();
        
    }

    private void finishOpeningSoundFile() {

        mWaveformView.setSoundFile(mSoundFile);
        mWaveformView.recomputeHeights(mDensity);

        mMaxPos = mWaveformView.maxPos();
        mLastDisplayedStartPos = -1;
        mLastDisplayedEndPos = -1;

        mTouchDragging = false;

        mOffset = 0;
        mOffsetGoal = 0;
        mFlingVelocity = 0;
        resetPositions();

        dismissProgessDialog();
        updateDisplay();
    }



    private String  TAG = EditAudio.class.getName();
    private String EXTENSION_MP3 = ".mp3";
    String KEY_SOUND_COLUMN_title = "title";
    String KEY_SOUND_COLUMN_path = "path";
   final int FILE_KIND_NOTIFICATION = 0;
   final int FILE_KIND_RINGTONE = 1;
   final   int FILE_KIND_Save = 2;
   final int FILE_KIND_ALARM = 3;
   final int FIlE_KIND_SHARE = 4;
   final int FILE_KIND_CONTACT = 5;

    private String getTimeFormat(String time){

        if (!time.isEmpty()){
            String Displayedmins ;
            String DisplayedSecs;
            double mins = java.lang.Double.parseDouble(time) % 3600 / 60;
            if (mins < 10) Displayedmins = "0" + String.valueOf((int)mins) ;
            else
                Displayedmins = String.valueOf((int)mins);
            double secs = java.lang.Double.parseDouble(time) % 60;
            if (secs < 10)
                DisplayedSecs = "0" + String.valueOf((int)secs);
            else
                DisplayedSecs = String.valueOf((int)secs);
            return Displayedmins + ":" + DisplayedSecs;
        }else
            return "";
    }


    @SuppressLint("NewApi")
     private synchronized void updateDisplay() {
        if (mPlayer != null) {
            mSoundDuration = mPlayer.getDuration() / 1000;

        }


        if (mIsPlaying) {


            float now = 0f;
            if (mPlayer != null) {
                now = (float) (mPlayer.getCurrentPosition() + mPlayStartOffset);
            }

            // check if the user has modified the limits
            int frames = mWaveformView.millisecsToPixels((int) now);
            mWaveformView.setPlayback(frames);
            setOffsetGoalNoUpdate(frames - mWidth / 2);




            if (now >= mPlayEndMsec) {
                handlePause();


            }
        }

        if (!mTouchDragging) {
            int offsetDelta;

            if (mFlingVelocity != 0) {

                offsetDelta = mFlingVelocity / 30;
                if (mFlingVelocity > 80) {
                    mFlingVelocity -= 80;
                } else if (mFlingVelocity < -80) {
                    mFlingVelocity += 80;
                } else {
                    mFlingVelocity = 0;
                }

                mOffset += offsetDelta;

                if (mOffset + mWidth / 2 > mMaxPos) {
                    mOffset = mMaxPos - mWidth / 2;
                    mFlingVelocity = 0;
                }
                if (mOffset < 0) {
                    mOffset = 0;
                    mFlingVelocity = 0;
                }
                mOffsetGoal = mOffset;
            } else {
                offsetDelta = mOffsetGoal - mOffset;

                if (offsetDelta > 10)
                    offsetDelta = offsetDelta / 10;
                else if (offsetDelta > 0)
                    offsetDelta = 1;
                else if (offsetDelta < -10)
                    offsetDelta = offsetDelta / 10;
                else if (offsetDelta < 0)
                    offsetDelta = -1;
                else
                    offsetDelta = 0;

                mOffset += offsetDelta;
            }
        }



        if (mWaveformView != null) {
            if (mWaveformView.getcurrentmLevel() != 0) {
                if (mWaveformView.getMeasuredWidth() + mOffset >= mWaveformView.getcurrentmLevel()) {
                    mOffset = mWaveformView.getcurrentmLevel() - mWaveformView.getMeasuredWidth();
                    EdgeReached = true;
                } else {
                    EdgeReached = false;
                }
            }

        }
        assert mWaveformView != null;
        mWaveformView.setParameters(mStartPos, mEndPos, mOffset, mSoundDuration);
        mWaveformView.invalidate();

        int startX = mStartPos - mOffset - mMarkerLeftInset;
        if (startX + mStartMarker.getWidth() >= 0) {
            if (!mStartVisible) {
                // Delay this to avoid flicker

                mHandler.postDelayed(() -> {
                    mStartVisible = true;
                    mStartMarker.setAlpha(1f);
                },0);

            }
        } else {
            if (mStartVisible) {
                mStartMarker.setAlpha(0f);
                mStartVisible = false;
            }
            startX = 0;
        }

        int endX = mEndPos - mOffset - mEndMarker.getWidth() + mMarkerRightInset;
        if (endX + mEndMarker.getWidth() >= 0) {
            if (!mEndVisible) {
                // Delay this to avoid flicker
                mHandler.postDelayed(()->{
                        mEndVisible = true;
                        mEndMarker.setAlpha(1f);

                }, 0);
            }
        } else {
            if (mEndVisible) {
                mEndMarker.setAlpha(0f);

                mEndVisible = false;
            }
            endX = 0;
        }


        RelativeLayout.LayoutParams layoutParamsStart = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);//(RelativeLayout.LayoutParams) mStartMarker.getLayoutParams();
       layoutParamsStart.setMargins(startX + marginvalue, mWaveformView.getMeasuredHeight(), 0, 0);

        mStartMarker.setLayoutParams( layoutParamsStart);
        RelativeLayout.LayoutParams layoutParamsEnd = (RelativeLayout.LayoutParams) mEndMarker.getLayoutParams();// new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        // if the marker does notification_ic reach the end  margin endx + value
        if (endX + marginvalue <= mWaveformView.getMeasuredWidth()) {
            layoutParamsEnd.setMargins(endX + marginvalue, mWaveformView.getMeasuredHeight(), 0, 0);
        } else {
            // if endx is less or equal the maxmium width we fix the margin at wave width
            if (endX <= mWaveformView.getMeasuredWidth()) {
                layoutParamsEnd.setMargins(mWaveformView.getMeasuredWidth(), mWaveformView.getMeasuredHeight(), 0, 0);
                // else we use the same endx as value for margin so it will disappear
            } else {
                layoutParamsEnd.setMargins(endX, mWaveformView.getMeasuredHeight(), 0, 0);
            }
        }

        mEndMarker.setLayoutParams(layoutParamsEnd);

    }


    private void enableDisableButtons() {

        runOnUiThread(() -> {
            if (mIsPlaying) {
                Play_Pause_View.toggle();
            } else {
                Play_Pause_View.toggle();
            }

        
        });
    }

    private void resetPositions() {
        mStartPos = 0;
        mEndPos = mMaxPos;
    }

    private int trap(int pos) {
        if (pos < 0)
            return 0;
        if (pos > mMaxPos)
             return mMaxPos ;
        else
             return pos;
    }

    private void setOffsetGoalStart() {
        setOffsetGoal(mStartPos - mWidth / 2);
    }
    private void setOffsetGoalStartNoUpdate() {
        setOffsetGoalNoUpdate(mStartPos - mWidth / 2);
    }
    private void setOffsetGoalEnd() {
        setOffsetGoal(mEndPos - mWidth / 2);
    }
    private void setOffsetGoalEndNoUpdate() {
        setOffsetGoalNoUpdate(mEndPos - mWidth / 2);
    }


    private void setOffsetGoal(int offset) {
        setOffsetGoalNoUpdate(offset);
        updateDisplay();
    }

    private void setOffsetGoalNoUpdate(int offset) {
        if (mTouchDragging) {
            return;
        }

        mOffsetGoal = offset;
        if (mOffsetGoal + mWidth / 2 > mMaxPos)
            mOffsetGoal = mMaxPos - mWidth / 2;
        if (mOffsetGoal < 0)
            mOffsetGoal = 0;
    }

    private String formatTime(int pixels) {
         if (mWaveformView != null && mWaveformView.isInitialized()) {
          return   formatDecimal(mWaveformView.pixelsToSeconds(pixels));
        } else {
           return  "";
        }
    }


    private String formatDecimal(double x) {
        int xWhole = (int) x;
        int xFrac = (int) (100 * (x - xWhole) + 0.5);

        if (xFrac >= 100) {
            xWhole++; //Round up
            xFrac -= 100 ;//Now we need the remainder after the round up
            if (xFrac < 10) {
                xFrac *= 10; //we need a fraction that is 2 digits long
            }
        }

         if (xFrac < 10)
             return xWhole+ ".0" + xFrac;
        else
             return xWhole + "." + xFrac;
    }

    
    private synchronized void handlePause(){
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
        }
        mWaveformView.setPlayback(-1);
        mIsPlaying = false;
        enableDisableButtons();
    }
    
    private synchronized void onPlay(int startPosition){
        if (mIsPlaying) {
            handlePause();
            return;
        }

        if (mPlayer == null) {
            // Not initialized yet
            return;
        }

        try {
            mPlayStartMsec = mWaveformView.pixelsToMillisecs(startPosition);
            if (startPosition < mStartPos) {
                mPlayEndMsec = mWaveformView.pixelsToMillisecs(mStartPos);
            } else if (startPosition > mEndPos) {
                mPlayEndMsec = mWaveformView.pixelsToMillisecs(mMaxPos);
            } else {
                mPlayEndMsec = mWaveformView.pixelsToMillisecs(mEndPos);
            }

            mPlayStartOffset = 0;

            int startFrame = mWaveformView.secondsToFrames(mPlayStartMsec * 0.001);
            int endFrame = mWaveformView.secondsToFrames(mPlayEndMsec * 0.001);

            int startByte = mSoundFile.getSeekableFrameOffset(startFrame);
            int endByte = mSoundFile.getSeekableFrameOffset(endFrame);
            if (startByte >= 0 && endByte >= 0) {
                try {
                    mPlayer.reset();
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                     FileInputStream subsetInputStream = new FileInputStream(mFile.getAbsolutePath());
                    mPlayer.setDataSource(subsetInputStream.getFD(), (long) startByte, (long) (endByte - startByte));
                    mPlayer.prepare();
                    mPlayStartOffset = mPlayStartMsec;
                } catch (Exception e) {
                    Log.e(TAG, "Exception trying to play file subset" + e);
                    mPlayer.reset();
                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mPlayer.setDataSource(mFile.getAbsolutePath());
                    mPlayer.prepare();
                    mPlayStartOffset = 0;
                }

            }
            
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    handlePause();
                    Log.d(TAG, "onCompletion: Completed");

                }
            });
            
            // mPlayer.setOnCompletionListener((MediaPlayer mediaPlayer) -> handlePause());
            mIsPlaying = true;

            if (mPlayStartOffset == 0) {
                mPlayer.seekTo(mPlayStartMsec);
            }
            mPlayer.start();
            updateDisplay();
            enableDisableButtons();
        } catch (Exception e) {
            //Log.e(TAG, "Exception while playing file" + e);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void CreateSelection(double startpoint, double endpoint) {
        if (mEndPos != -1 || mStartPos != -1) {

            float endpointbefore = (float) mWaveformView.pixelsToSeconds(mEndPos);
                    float endpointafter = (float) endpoint;


                    PropertyValuesHolder propertyValuesHolder =  PropertyValuesHolder.ofFloat("phase", endpointbefore,endpointafter);


            float startpointBefore = java.lang.Float.parseFloat(String.valueOf(mWaveformView.pixelsToSeconds(mStartPos)));
                    float startpointAFter = (float) startpoint;
                    PropertyValuesHolder propertyValuesHolder2 = PropertyValuesHolder.ofFloat("phase2", startpointBefore, startpointAFter);


            ObjectAnimator mObjectAnimator =  ObjectAnimator.ofPropertyValuesHolder(this,propertyValuesHolder, propertyValuesHolder2);

            mObjectAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float newEndpos = (float) valueAnimator.getAnimatedValue(propertyValuesHolder.getPropertyName());
                    mEndPos = mWaveformView.secondsToPixels(newEndpos);
                    float NewStartpos = (float) valueAnimator.getAnimatedValue(propertyValuesHolder2.getPropertyName());

                    mStartPos = mWaveformView.secondsToPixels(NewStartpos);
                    mStartText.setText(""+(newEndpos % 3600 / 60) + ":" + (newEndpos % 60));
                    mEndText.setText(""+(NewStartpos % 3600 / 60)+ ":" + (NewStartpos % 60));
                    updateDisplay();
                }
            });


            mObjectAnimator.start();

            mStartText.setText(""+((int)(startpoint % 3600 / 60))+ ":" + ((int) (startpoint % 60)));
            mEndText.setText( ""+((int)(endpoint % 3600 / 60)) + ":" + (int) (endpoint % 60));
            mEndPos = mWaveformView.secondsToPixels(endpoint);
            mStartPos = mWaveformView.secondsToPixels(startpoint);
            updateDisplay();


        }
    }

    void setPhase(float phase) {

    }
    void setPhase2(float phase2) {

    }


    @Override
    public void onClick(View view) {

        if (zoom_in.equals(view)) {
            waveformZoomIn();
        }else if (zoom_out.equals(view)){
            waveformZoomOut();
        }else if (Button_Done.equals(view)){
            HandleCutRequest();
        }else if (image_Cancel.equals(view)){
            runanimation();
        }else if (Play_Pause_View.equals(view)){
            onPlay(mStartPos);
        }else if (mark_start.equals(view)){
            if (mIsPlaying) {
                mStartPos = mWaveformView.millisecsToPixels(mPlayer.getCurrentPosition() + mPlayStartOffset);
                updateDisplay();
            }
        }else if (mark_end.equals(view)){
            mEndPos = mWaveformView.millisecsToPixels(mPlayer.getCurrentPosition() + mPlayStartOffset);
            updateDisplay();
            handlePause();
        }else {
            Cutselection(view.getId());
        }

    }

    private void HandleCutRequest() {
        if (mPlayer.isPlaying()) handlePause();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(EditAudio.this)) {
                //                com.zatrek.zatrekcut.Pixels.Log.d(TAG, "onClick: Permission Done");
                runanimation();
            } else {
                openAndroidPermissionsMenu();
            }
        } else {
            runanimation();
        }
    }

    private void openAndroidPermissionsMenu() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }


    void runanimation() {

        LinearLayout optionsContainer = findViewById(R.id.options_container);
        int[] array = new int[2];
        Button_Done.getLocationOnScreen(array);

        optionsContainer.post(()->{

                int cx = Pixels.getScreenWidth(this) / 2;
                int cy = array[1];
                int radius = Math.max(optionsContainer.getWidth(), optionsContainer.getHeight());

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {


                    SupportAnimator animator = ViewAnimationUtils.createCircularReveal(optionsContainer, cx, cy, 0f, radius);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    SupportAnimator animator_reverse = animator.reverse();

                    if (Maskhidden) {
                        optionsContainer.setVisibility( View.VISIBLE);
                        animator.start();
                        Maskhidden = false;
                        editor_container.setVisibility(View.INVISIBLE);
                    } else {

                        animator_reverse.addListener(new SupportAnimator.AnimatorListener() {
                            @Override
                            public void onAnimationStart() {

                            }

                            @Override
                            public void onAnimationEnd() {
                                optionsContainer.setVisibility(View.INVISIBLE);
                                editor_container.setVisibility(View.VISIBLE);
                                Maskhidden = true;
                            }

                            @Override
                            public void onAnimationCancel() {

                            }

                            @Override
                            public void onAnimationRepeat() {

                            }
                        });
                        animator_reverse.start();
                    }
                } else {

                    if (Maskhidden) {
                        optionsContainer.setVisibility(View.VISIBLE);
                        optionsContainer.requestLayout();
                        android.view.ViewAnimationUtils.createCircularReveal(optionsContainer, cx, cy, 0f, radius).start();
                        Maskhidden = false;
                        editor_container.setVisibility(View.INVISIBLE);
                    } else {
                        Animator anim = android.view.ViewAnimationUtils.createCircularReveal(optionsContainer, cx, cy, radius, 0f);

                        anim.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationCancel(Animator animation) {
                                super.onAnimationCancel(animation);
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);

                                optionsContainer.setVisibility(View.INVISIBLE);
                                Maskhidden = true;
                                editor_container.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                                super.onAnimationRepeat(animation);
                            }

                            @Override
                            public void onAnimationStart(Animator animation) {
                                super.onAnimationStart(animation);
                            }

                            @Override
                            public void onAnimationPause(Animator animation) {
                                super.onAnimationPause(animation);
                            }

                            @Override
                            public void onAnimationResume(Animator animation) {
                                super.onAnimationResume(animation);
                            }
                        });
                        anim.start();
                    }
                }
        });
    }

    void Cutselection(int which){

            if (which == R.id.Editor_Ringtone) {
                SaveRingTone();
                mNewFileKind = FILE_KIND_RINGTONE;
            }else if ( R.id.Editor_Notification==which){
                SaveRingTone();
                mNewFileKind = FILE_KIND_NOTIFICATION;
            }else
            if (R.id.Editor_Save==which) {
                SaveRingTone();
                mNewFileKind = FILE_KIND_Save;
            }else
             if (R.id.Editor_Alarm==which){
                SaveRingTone();
                mNewFileKind = FILE_KIND_ALARM;
            }else
            if (R.id.Editor_Contacts==which) {
                if (PermissionManager.checkAndRequestContactsPermissions(this)) {
                    //chooseContactForRingtone();
                    SaveRingTone();
                    mNewFileKind = FILE_KIND_CONTACT;
                }
            }else if (R.id.Editor_Share==which){
                SaveRingTone();
                mNewFileKind = FIlE_KIND_SHARE;
            }
    }

    private void chooseContactForRingtone(Uri newUri) {
        Intent intent = new Intent(this, Activity_ContactsChoice.class);
        intent.putExtra("FILE_NAME", newUri);
        this.startActivity(intent);
    }

    private void SaveRingTone() {

        double startTime = mWaveformView.pixelsToSeconds(mStartPos);
        double endTime = mWaveformView.pixelsToSeconds(mEndPos);

        int mStartPosMilliS = mWaveformView.pixelsToMillisecs(mStartPos);
        int mEndPosMilliS = mWaveformView.pixelsToMillisecs(mEndPos);

        int startFrame = mWaveformView.secondsToFrames(mStartPosMilliS * 0.001);
        int numFrames = mWaveformView.secondsToFrames(mEndPosMilliS * 0.001) - startFrame;

        int fadeTime = mWaveformView.secondsToFrames(5.0);
        int duration = (int) (endTime - startTime + 0.5);


        // Create an indeterminate progress dialog
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setTitle(getString(R.string.dialog_saving));
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);

        runOnUiThread(()-> mProgressDialog.show());
        //?: return
        // Write the new file
        // log the error and try to create a .wav file instead
        // Creating the .wav file also failed. Stop the progress dialog, show an
        // error message and exit.
         mSaveSoundFileThread = new Thread() {
            @Override
            public void run() {
               // super.run();
                String outPath = makeRingtoneFilename(Editor_song_title.getText().toString(), ""+mFile.getAbsolutePath().substring(mFile.getAbsolutePath().lastIndexOf(".")));
                //?: return
                outputFile = new File(outPath);
                boolean fallbackToWAV = false;
                try {
                    // Write the new file
                    mSoundFile.WriteFile(outputFile, startFrame, numFrames, false, false, fadeTime);


                } catch (Exception e) {
                    // log the error and try to create a .wav file instead
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    e.printStackTrace();
                    StringWriter writer = new StringWriter();
                    e.printStackTrace(new PrintWriter(writer));
                    fallbackToWAV = true;
                }

                if (fallbackToWAV) {
                    outPath = makeRingtoneFilename(Editor_song_title.getText().toString(), ".wav");
                    if (outPath == null) {
                        Runnable runnable = () -> {
                        };
                        mHandler.post(runnable);
                        return;
                    }
                    outputFile = new File(outPath);
                    try {
                        mSoundFile.writewavfile(outputFile, startFrame, numFrames, false, false, fadeTime);


                    } catch (Exception e) {
                        // Creating the .wav file also failed. Stop the progress dialog, show an
                        // error message and exit.
                        dismissProgessDialog();
                        if (outputFile.exists()) {
                            outputFile.delete();
                        }

                        return;
                    }

                }

                String finalOutPath = outPath;
                Runnable runnable = () -> afterSavingRingtone(Editor_song_title.getText().toString(),
                        finalOutPath,
                        duration, endTime);
                mHandler.post(runnable);
                dismissProgessDialog();
            }

        };
        mSaveSoundFileThread.start();
    }

    private void afterSavingRingtone(CharSequence title, String outPath, int duration, double endpoint ) {


       // val dbHelper = com.ringtone.maker.Database.DBHelper.getInstance(this);
        File outFile = new File(outPath);
        long fileSize = outFile.length();
        if (fileSize <= 512) {
            outFile.delete();
           new  AlertDialog.Builder(this)
                    .setTitle("Failure")
                    .setMessage("File is too Small")
                    .setPositiveButton("Ok", null)
                    .setCancelable(false)
                    .show();
            return;
        }

        // Create the database record, pointing to the existing file path
        String mimeType;
        if (outPath.endsWith(".m4a")){
            mimeType = "audio/mp4a-latm";
        }else if (outPath.endsWith(".wav")){
            mimeType = "audio/wav";
        }else {
            mimeType = "audio/mpeg";
        }


        String artist = "Music XPro Audio Cutter";

        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, outPath);
        values.put(MediaStore.MediaColumns.TITLE, title.toString());
        values.put(MediaStore.MediaColumns.SIZE, fileSize);
        values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);
        values.put(MediaStore.Audio.Media.ARTIST, artist);
        values.put(MediaStore.Audio.Media.DURATION, duration);
        values.put(MediaStore.Audio.Media.IS_MUSIC, true);
        Uri uri = MediaStore.Audio.Media.getContentUriForPath(outPath);
        Uri newUri = getContentResolver().insert(uri, values);
        setResult(Activity.RESULT_OK, new Intent().setData(newUri));




        if (mNewFileKind==FILE_KIND_NOTIFICATION){
            RingtoneManager.setActualDefaultRingtoneUri(this,RingtoneManager.TYPE_NOTIFICATION,newUri);
        }else if (mNewFileKind == FILE_KIND_RINGTONE){
            RingtoneManager.setActualDefaultRingtoneUri(this, RingtoneManager.TYPE_RINGTONE, newUri);
        }else if (FILE_KIND_ALARM == mNewFileKind){
            RingtoneManager.setActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM, newUri);
        }else if (FILE_KIND_Save == mNewFileKind){

        }else if (FIlE_KIND_SHARE == mNewFileKind){
            shareAudioFile(newUri);
        }else if (FILE_KIND_CONTACT==mNewFileKind){
            chooseContactForRingtone(newUri);
        }

        MediaScannerConnection.scanFile(this, new String[] { newUri.getPath() }, null,
                (path, uri1) -> {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri1);
                });

        Snacky.builder()
                .setActivty(this)
                .setBackgroundColor(ContextCompat.getColor(this,R.color.editor_toast_color))
                .setText(this.getString(R.string.Edit_Done_Toast))
                .setDuration(Snacky.LENGTH_LONG)
                .success().show();

    }


    private String makeRingtoneFilename(CharSequence title,String extension) {

        String subdir = null;


        subdir = "Audio_Cutter";



        String extr = Environment.getExternalStorageDirectory().toString();
        String externalRootDir = extr + "/" + "Music_XPro" + "/" + subdir;
        if (!externalRootDir.endsWith("/")) {
            externalRootDir += "/";
        }

        String parentdir = externalRootDir;
        // Create the parent directory
        File parentDirFile = new File(parentdir);
        parentDirFile.mkdirs();

        // If we can't write to that special path, try just writing
        // directly to the sdcard
        if (!parentDirFile.isDirectory()) {
            parentdir = externalRootDir;
        }

        // Turn the title into a filename
        CharSequence filename = "".toString() ;
        for (int i = 0 ; i< title.length() ; i++){
            if (Character.isLetterOrDigit(title.charAt(i))) {
                filename = filename + ""+title.charAt(i);
            }
        }

        // Try to make the filename unique
        String path = "";
        for (int i = 0 ;i<99;i++) {
            String testPath;
            if (i > 0)
                testPath = parentdir + filename + i + extension;
            else
                testPath = parentdir + filename + extension;

            try {
                RandomAccessFile f = new RandomAccessFile(new File(testPath), "r");
                f.close();
            } catch (Exception e) {
                // Good, the file didn't exist
                path = testPath;
                break;
            }

        }

        return path;
    }




    public void dismissProgessDialog(){
        runOnUiThread(() -> mProgressDialog.dismiss());
    }

    public void shareAudioFile(Uri path){
        //FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName(), new File(path)
        try {
           Intent intent =  new Intent()
                    .setAction(Intent.ACTION_SEND)
                    .putExtra(Intent.EXTRA_STREAM, path)
                    .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    .setType("audio/*");
             startActivity(intent);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Could not share this file, I'm aware of the issue.", Toast.LENGTH_SHORT).show();
        }
    }
}