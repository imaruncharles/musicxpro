package com.onecupcode.audioeditor.worker;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;

import com.google.common.util.concurrent.ListenableFuture;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

import java.io.File;

import timber.log.Timber;

import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_CANCEL;
import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_SUCCESS;


public class AudioMergeWork extends ListenableWorker {
    /**
     * @param appContext   The application {@link Context}
     * @param workerParams Parameters to setup the internal state of this worker
     */

    public AudioMergeWork(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
        super(appContext, workerParams);
    }


        //-i /storage/emulated/0/Download/videoplayback.mp4 -q:a 0 -map a /sdcard/Download/youtubedl-android/file2.wav
        //  String[] command = getIntent().getStringExtra("commend").split(" "); //{"-i", "/storage/emulated/0/Download/videoplayback.mp4", /*"-q:a", "0", "-map", "a",*/ "/sdcard/Download/youtubedl-android/file8.mkv"};

    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {
        return  CallbackToFutureAdapter.getFuture(completer ->{
            ProgressManager progressManager= new ProgressManager(getApplicationContext());

            Timber.d(getInputData().getString("filename")+" "+"on start");
            progressManager.updateProgress("Audio Merging Process Running..",getInputData().getString("filename"),true,-1,getInputData().getString("outputpath"));
          com.arthenica.mobileffmpeg.FFmpeg.executeAsync(getInputData().getStringArray("command"), (executionId1, returnCode) -> {
              if (returnCode== RETURN_CODE_SUCCESS){
                  progressManager.updateProgress("Audio Merging Done",getInputData().getString("filename"),false,0,getInputData().getString("outputpath"));
                  mediaScanner(getInputData().getString("outputpath"),getInputData().getString("filename"), completer);

              }else if (returnCode == RETURN_CODE_CANCEL){
                  progressManager.updateProgress("Audio Merging Canceled",getInputData().getString("filename"),false,1,getInputData().getString("outputpath"));
              completer.set(Result.failure());
              }else {
                  progressManager.updateProgress("Audio Merging Failed",getInputData().getString("filename"),false,2,getInputData().getString("outputpath"));
                completer.set(Result.failure());
              }
          });



          return null;//completer.set(Result.success());
        });
    }






    private void mediaScanner(String out_path, String title, CallbackToFutureAdapter.Completer<Result> completer){
        try {

            String artist = "Music XPro";
            AudioFile f = AudioFileIO.read(new File(out_path));
            Tag tag = f.getTag();
            tag.setField(FieldKey.ARTIST,artist);
            tag.setField(FieldKey.TITLE,title);
            AudioFileIO.write(f);



            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                final Uri contentUri = Uri.parse(out_path);
                scanIntent.setData(contentUri);
               getApplicationContext().sendBroadcast(scanIntent);
            } else {
                final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()));
                getApplicationContext().sendBroadcast(intent);
            }


        MediaScannerConnection.scanFile(getApplicationContext(), new String[] { out_path }, null,
                (path, uri1) -> {
                    Timber.i("Scanned " + path + ":");
                    Timber.i("-> uri=%s", uri1);
                });
        }catch (Exception ignore){

        }finally {
            completer.set(Result.success());
        }


    }



}
