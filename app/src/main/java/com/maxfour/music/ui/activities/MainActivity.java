package com.maxfour.music.ui.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.kabouzeid.appthemehelper.ThemeStore;
import com.kabouzeid.appthemehelper.util.ATHUtil;
import com.kabouzeid.appthemehelper.util.NavigationViewUtil;
import com.maxfour.music.BuildConfig;
import com.maxfour.music.R;
import com.maxfour.music.dialogs.ScanMediaFolderChooserDialog;
import com.maxfour.music.dialogs.SleepTimerDialog;
import com.maxfour.music.glide.SongGlideRequest;
import com.maxfour.music.helper.MusicPlayerRemote;
import com.maxfour.music.helper.SearchQueryHelper;
import com.maxfour.music.loader.AlbumLoader;
import com.maxfour.music.loader.ArtistLoader;
import com.maxfour.music.loader.PlaylistSongLoader;
import com.maxfour.music.model.Playlist;
import com.maxfour.music.model.Song;
import com.maxfour.music.model.smartplaylist.LastAddedPlaylist;
import com.maxfour.music.service.MusicService;
import com.maxfour.music.ui.activities.base.AbsSlidingMusicPanelActivity;
import com.maxfour.music.ui.activities.editor.EditorOutput;
import com.maxfour.music.ui.activities.intro.AppIntroActivity;
import com.maxfour.music.ui.fragments.mainactivity.folders.FoldersFragment;
import com.maxfour.music.ui.fragments.mainactivity.library.LibraryFragment;
import com.maxfour.music.ui.fragments.mainactivity.quickfolder.QuickFoldersFragment;
import com.maxfour.music.util.MusicUtil;
import com.maxfour.music.util.NavigationUtil;
import com.maxfour.music.util.PreferenceUtil;
import com.onecupcode.audioeditor.AudioMerger;
import com.onecupcode.audioeditor.audiocutter.AudioListActivity;
import com.onecupcode.audioeditor.audiocutter.RingdroidSelectActivity;
import com.onecupcode.audioeditor.videotoaudio.VideoToAudio;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AbsSlidingMusicPanelActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static final int APP_INTRO_REQUEST = 100;

    private static final int LIBRARY = 0;
    private static final int FOLDERS = 1;
    private static final int QUICKFOLDER = 3;

    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Nullable
    MainActivityFragmentCallbacks currentFragment;

    @Nullable
    private View navigationDrawerHeader;

    private boolean blockRequestPermissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDrawUnderStatusbar();
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            navigationView.setFitsSystemWindows(false); // for header to go below statusbar
        }

        setUpDrawerLayout();

        if (savedInstanceState == null) {
            setMusicChooser(PreferenceUtil.getInstance(this).getLastMusicChooser());
        } else {
            restoreCurrentFragment();
        }

        if (!checkShowIntro()) {

        }
    }

    private void setMusicChooser(int key) {

        PreferenceUtil.getInstance(this).setLastMusicChooser(key);
        switch (key) {
            case LIBRARY:
                navigationView.setCheckedItem(R.id.nav_library);
                setCurrentFragment(LibraryFragment.newInstance());
                break;
            case FOLDERS:
                navigationView.setCheckedItem(R.id.nav_folders);
                setCurrentFragment(FoldersFragment.newInstance(this));
                break;
            case QUICKFOLDER:
                navigationView.setCheckedItem(R.id.nav_quick_access);
                setCurrentFragment(QuickFoldersFragment.newInstance(this));
        }
    }

    private void setCurrentFragment(@SuppressWarnings("NullableProblems") Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment, null).commit();
        currentFragment = (MainActivityFragmentCallbacks) fragment;
    }

    private void restoreCurrentFragment() {
        currentFragment = (MainActivityFragmentCallbacks) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_INTRO_REQUEST) {
            blockRequestPermissions = false;
            if (!hasPermissions()) {
                requestPermissions();
            }else {
                new AsyncTaskShare().execute();
            }
        }
    }

    @Override
    protected void requestPermissions() {
        if (!blockRequestPermissions) super.requestPermissions();
    }

    @Override
    protected View createContentView() {
        @SuppressLint("InflateParams")
        View contentView = getLayoutInflater().inflate(R.layout.activity_main_drawer_layout, null);
        ViewGroup drawerContent = contentView.findViewById(R.id.drawer_content_container);
        drawerContent.addView(wrapSlidingMusicPanel(R.layout.activity_main_content));
        return contentView;
    }

    private void setUpNavigationView() {
        int accentColor = ThemeStore.accentColor(this);
        NavigationViewUtil.setItemIconColors(navigationView, ATHUtil.resolveColor(this, R.attr.iconColor, ThemeStore.textColorSecondary(this)), accentColor);
        NavigationViewUtil.setItemTextColors(navigationView, ThemeStore.textColorPrimary(this), accentColor);

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            drawerLayout.closeDrawers();
            switch (menuItem.getItemId()) {
                case R.id.nav_library:
                    new Handler().postDelayed(() -> setMusicChooser(LIBRARY), 200);
                    break;
                case R.id.nav_folders:
                    new Handler().postDelayed(() -> setMusicChooser(FOLDERS), 200);
                    break;
                case R.id.nav_quick_access:
                    new Handler().postDelayed(()->setMusicChooser(QUICKFOLDER),200);
                    break;
                case R.id.action_scan:
                    new Handler().postDelayed(() -> {
                        ScanMediaFolderChooserDialog dialog = ScanMediaFolderChooserDialog.create();
                        dialog.show(getSupportFragmentManager(), "SCAN_MEDIA_FOLDER_CHOOSER");
                    }, 200);
                    break;
                case R.id.nav_equalizer:
                    // NavigationUtil.openEqualizer(this); // don't delete important
                     startActivity(new Intent(getApplicationContext(), Equalizer.class));
                    break;
                case R.id.nav_last_added:
                    //startActivity(new Intent(getApplicationContext(), EditorOutput.class));
                    NavigationUtil.goToPlaylist(MainActivity.this,new LastAddedPlaylist(getApplicationContext()));
                    break;

                case R.id.nav_audio_merger:
                    MusicPlayerRemote.pauseSong();
                    startActivity(new Intent(getApplicationContext(), AudioMerger.class).putExtra("build",BuildConfig.APPLICATION_ID));
                    break;
                case R.id.nav_sleepTimer:
                    openSleepTimer();
                    break;
                case R.id.nav_settings:
                    new Handler().postDelayed(() -> startActivity(new Intent(MainActivity.this, SettingsActivity.class)), 200);
                    break;
                case R.id.nav_about:
                    new Handler().postDelayed(() -> startActivity(new Intent(MainActivity.this, AboutActivity.class)), 200);
                    break;
                case R.id.nav_share:
                    try {
                        shareThisApp();
                    }catch (Exception ignored){

                    }
                    break;
                case R.id.nav_audio_cutter:
                    MusicPlayerRemote.pauseSong();
                    startActivity(new Intent(getApplicationContext(), AudioListActivity.class).putExtra("build",BuildConfig.APPLICATION_ID));
                    break;
                case R.id.nav_rate:
                   // rateThisApp();
                    ratethisApp();
                    break;

                case R.id.nav_videotoaudio:
                    MusicPlayerRemote.pauseSong();
                    startActivity(new Intent(getApplicationContext(), VideoToAudio.class).putExtra("build",BuildConfig.APPLICATION_ID));
                    break;
                case R.id.nav_feed:
                    sendFeedback();
                    break;
            }
            return true;
        });
    }


    public  void openSleepTimer(){
        new SleepTimerDialog().show(this.getSupportFragmentManager(), "SET_SLEEP_TIMER");
    }

    private void setUpDrawerLayout() {
        setUpNavigationView();
    }

    private void updateNavigationDrawerHeader() {
        if (!MusicPlayerRemote.getPlayingQueue().isEmpty()) {
            Song song = MusicPlayerRemote.getCurrentSong();
            if (navigationDrawerHeader == null) {
                navigationDrawerHeader = navigationView.inflateHeaderView(R.layout.navigation_drawer_header);
                //noinspection ConstantConditions
                navigationDrawerHeader.setOnClickListener(v -> {
                    drawerLayout.closeDrawers();
                    if (getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                        expandPanel();
                    }
                });
            }
            ((TextView) navigationDrawerHeader.findViewById(R.id.title)).setText(song.title);
            ((TextView) navigationDrawerHeader.findViewById(R.id.text)).setText(MusicUtil.getSongInfoString(song));
            SongGlideRequest.Builder.from(Glide.with(this), song)
                    .checkIgnoreMediaStore(this).build()
                    .into(((ImageView) navigationDrawerHeader.findViewById(R.id.image)));
        } else {
            if (navigationDrawerHeader != null) {
                navigationView.removeHeaderView(navigationDrawerHeader);
                navigationDrawerHeader = null;
            }
        }
    }

    @Override
    public void onPlayingMetaChanged() {
        super.onPlayingMetaChanged();
        updateNavigationDrawerHeader();
    }

    @Override
    public void onServiceConnected() {
        super.onServiceConnected();
        updateNavigationDrawerHeader();
        handlePlaybackIntent(getIntent());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (drawerLayout.isDrawerOpen(navigationView)) {
                drawerLayout.closeDrawer(navigationView);
            } else {
                drawerLayout.openDrawer(navigationView);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean handleBackPress() {
        if (drawerLayout.isDrawerOpen(navigationView)) {
            drawerLayout.closeDrawers();
            return true;
        }
        return super.handleBackPress() || (currentFragment != null && currentFragment.handleBackPress());
    }

    private void handlePlaybackIntent(@Nullable Intent intent) {
        if (intent == null) {
            return;
        }

        Uri uri = intent.getData();
        String mimeType = intent.getType();
        boolean handled = false;

        if (intent.getAction() != null && intent.getAction().equals(MediaStore.INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH)) {
            final List<Song> songs = SearchQueryHelper.getSongs(this, intent.getExtras());
            if (MusicPlayerRemote.getShuffleMode() == MusicService.SHUFFLE_MODE_SHUFFLE) {
                MusicPlayerRemote.openAndShuffleQueue(songs, true);
            } else {
                MusicPlayerRemote.openQueue(songs, 0, true);
            }
            handled = true;
        }

        if (uri != null && uri.toString().length() > 0) {
            MusicPlayerRemote.playFromUri(uri);
            handled = true;
        } else if (MediaStore.Audio.Playlists.CONTENT_TYPE.equals(mimeType)) {
            final int id = (int) parseIdFromIntent(intent, "playlistId", "playlist");
            if (id >= 0) {
                int position = intent.getIntExtra("position", 0);
                List<Song> songs = new ArrayList<>(PlaylistSongLoader.getPlaylistSongList(this, id));
                MusicPlayerRemote.openQueue(songs, position, true);
                handled = true;
            }
        } else if (MediaStore.Audio.Albums.CONTENT_TYPE.equals(mimeType)) {
            final int id = (int) parseIdFromIntent(intent, "albumId", "album");
            if (id >= 0) {
                int position = intent.getIntExtra("position", 0);
                MusicPlayerRemote.openQueue(AlbumLoader.getAlbum(this, id).songs, position, true);
                handled = true;
            }
        } else if (MediaStore.Audio.Artists.CONTENT_TYPE.equals(mimeType)) {
            final int id = (int) parseIdFromIntent(intent, "artistId", "artist");
            if (id >= 0) {
                int position = intent.getIntExtra("position", 0);
                MusicPlayerRemote.openQueue(ArtistLoader.getArtist(this, id).getSongs(), position, true);
                handled = true;
            }
        }
        if (handled) {
            setIntent(new Intent());
        }
    }

    private long parseIdFromIntent(@NonNull Intent intent, String longKey,
                                   String stringKey) {
        long id = intent.getLongExtra(longKey, -1);
        if (id < 0) {
            String idString = intent.getStringExtra(stringKey);
            if (idString != null) {
                try {
                    id = Long.parseLong(idString);
                } catch (NumberFormatException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }
        return id;
    }

    @Override
    public void onPanelExpanded(View view) {
        super.onPanelExpanded(view);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onPanelCollapsed(View view) {
        super.onPanelCollapsed(view);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private boolean checkShowIntro() {
        if (!PreferenceUtil.getInstance(this).introShown()) {
            PreferenceUtil.getInstance(this).setIntroShown();
            blockRequestPermissions = true;
            new Handler().postDelayed(() -> startActivityForResult(new Intent(MainActivity.this, AppIntroActivity.class), APP_INTRO_REQUEST), 0);
            return true;
        }
        return false;
    }

    public interface MainActivityFragmentCallbacks {
        boolean handleBackPress();
    }

    public void shareThisApp(){
        Uri bitmapUri= getBitmapFromDrawable();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
        intent.putExtra(Intent.EXTRA_TEXT, "Hey! Try this \"Music XPro\" Music player app with awesome UI, equalizer, automatic sleep timer, playlist and much more loved it. \uD83E\uDD70 " + "https://play.google.com/store/apps/details?id=" + getPackageName());
        startActivity(Intent.createChooser(intent, "Share Music XPro"));
    }


    class AsyncTaskShare extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... strings) {
            try {
                getBitmapFromDrawable();

            }catch (Exception ignored){

            }


            return null;
        }
    }

    // Method when launching drawable within Glide
    public Uri getBitmapFromDrawable(){
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {

            //getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            File file =  new File(getCacheDir(), "share_image_music_xpro.png");
            if (!file.exists()){
               Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.music_xpro_new1);
                FileOutputStream out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
            }

            // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
            bmpUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, file);  // use this version for API >= 24

            // **Note:** For API < 24, you may use bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }



    /*public void initThisApp(){

    }

    public void rateThisApp(){
        ReviewManager manager = ReviewManagerFactory.create(getApplicationContext());
        Task<ReviewInfo> request = manager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                ReviewInfo reviewInfo = task.getResult();
                Task<Void> flow = manager.launchReviewFlow(MainActivity.this, reviewInfo);
                flow.addOnCompleteListener(
                        task1 -> {
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                });
            }else {
                Toast.makeText(this, "Review", Toast.LENGTH_SHORT).show();
            }
        });
    }*/


    public void ratethisApp(){

        try {
            Uri uriAlter = Uri.parse("http://play.google.com/store/apps/details?id="+BuildConfig.APPLICATION_ID);
            Intent intent = new Intent(Intent.ACTION_VIEW,uriAlter);
            if (intent.resolveActivity(getPackageManager())!=null){
                startActivity(intent);
            }
        }catch (ActivityNotFoundException ignore){

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (PERMISSION_REQUEST==requestCode){
            if (grantResults[0]== PackageManager.PERMISSION_GRANTED){
                new AsyncTaskShare().execute();
            }
        }

    }


    public void sendFeedback()
    {
        Intent email = new Intent(Intent.ACTION_SENDTO);
        email.setData(Uri.parse("mailto:"));
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "onecupcode@gmail.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Music XPro - Paid");
        email.putExtra(Intent.EXTRA_TEXT, "");

        //need this to prompts email client only


        if (email.resolveActivity(getPackageManager())!=null){
            startActivity(Intent.createChooser(email, "Choose an Email client :"));
        }


    }

}
